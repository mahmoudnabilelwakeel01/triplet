<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Reservation;
use App\Models\Transportation;
use App\Models\User;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    // service
    public function service()
    {
        return view('admin.service');
    }
    // service transportation
    public function serv_transportation()
    {
        $transportations = Transportation::get();
        return view('admin.serv_transportation', compact('transportations'));
    }
    // service transportation // add
    public function serv_transportation_add()
    {
        return view('admin.serv_transportation_add');
    }
    // service transportation // store
    public function serv_transportation_store(Request $request)
    {
        $request->validate([
            "name" => ['required'],
            "photo" => ['required'],
            "link" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            $photo = $request->photo->store('/public/transportation');
            // store logic
            Transportation::create([
                'user_id' => Auth()->guard('web')->user()->id,
                'name' => $request->name,
                'photo' => $photo,
                'link' => $request->link,
            ]);
            return redirect()->route('admin.serv_transportation');
        } else {
            return redirect()->back();
        }
    }
    // service transportation // edit
    public function serv_transportation_edit($id)
    {
        $transportation = Transportation::findOrFail($id);
        return view('admin.serv_transportation_edit', compact('transportation'));
    }
    // service transportation // update
    public function serv_transportation_update($id, Request $request)
    {
        $transportation = Transportation::findOrFail($id);
        $request->validate([
            "name" => ['required'],
            "link" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            if ($transportation->photo) {
                \Storage::delete($transportation->photo);
            }
            $photo = $request->photo->store('/public/transportation');
        } else {
            $photo = $transportation->photo;
        }
        // update logic
        $transportation->update([
            'name' => $request->name,
            'photo' => $photo,
            'link' => $request->link,
        ]);
        return redirect()->route('admin.serv_transportation');
    }
    // service  transportation delete
    public function serv_transportation_delete($id)
    {
        $transportation = Transportation::find($id);
        \Storage::delete($transportation->photo);
        $transportation->delete();
        return view('admin.serv_transportation_delete');
    }

    // service reservation
    public function serv_reservation()
    {
        $reservations = Reservation::get();
        return view('admin.serv_reservation', compact('reservations'));
    }
    // service reservation // add
    public function serv_reservation_add()
    {
        return view('admin.serv_reservation_add');
    }
    // service reservation // store
    public function serv_reservation_store(Request $request)
    {
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "photo" => ['required'],
            "location" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            $photo = $request->photo->store('/public/reservation');
            // store logic
            Reservation::create([
                'user_id' => Auth()->guard('web')->user()->id,
                'type' => $request->type,
                'name' => $request->name,
                'city' => $request->city,
                'photo' => $photo,
                'location' => $request->location,
            ]);
            return redirect()->route('admin.serv_reservation');
        } else {
            return redirect()->back();
        }
    }

    // service reservation // edit
    public function serv_reservation_edit($id)
    {
        $reservation = Reservation::findOrFail($id);
        return view('admin.serv_reservation_edit', compact('reservation'));
    }
    // service reservation // update
    public function serv_reservation_update($id, Request $request)
    {
        $reservation = Reservation::findOrFail($id);
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "location" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            if ($reservation->photo) {
                \Storage::delete($reservation->photo);
            }
            $photo = $request->photo->store('/public/reservation');
        } else {
            $photo = $reservation->photo;
        }
        // update logic
        $reservation->update([
            'type' => $request->type,
            'name' => $request->name,
            'city' => $request->city,
            'photo' => $photo,
            'location' => $request->location,
        ]);
        return redirect()->route('admin.serv_reservation');
    }
    // service reservation delete
    public function serv_reservation_delete($id)
    {
        $reservation = Reservation::find($id);
        \Storage::delete($reservation->photo);
        $reservation->delete();
        return view('admin.serv_reservation_delete');
    }




    // contact
    public function contact()
    {
        $contacts = Contact::get();
        return view('admin.contact', compact('contacts'));
    }
    // contact response
    public function contactresponse($id)
    {
        $contact = Contact::findOrFail($id);
        return view('admin.contactresponse', compact('contact'));
    }
    // contact response save
    public function contactresponsesave(Request $request, $id)
    {
        $contact = Contact::findOrFail($id);
        $contact->update([
            'respnose' => $request->respnose,
        ]);
        return redirect()->route('admin.contact');
    }
    // contact delete
    public function contactdelete($id)
    {
        Contact::where('id', $id)->delete();
        $contacts = Contact::get();
        return view('admin.contact', compact('contacts'));
    }
}
