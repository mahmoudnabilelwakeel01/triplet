<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    public function loginPage()
    {
        if (Auth::check()) {
            Auth::guard('web')->logout();
        }

        return view('admin.auth.login');
    }

    public function login(Request $request)
    {
        // dd(Hash::make($request->password));
        $credentials = $request->only('email', 'password');
        if (Auth::guard('web')->attempt($credentials)) {
            $request->session()->regenerate();
            if (Auth::user()->type == 1) {
                return redirect()->intended('/admin/service');
            } else {
                return redirect()->intended('/');
            }

        } else {
            return view('admin.auth.login');
        }
        // $credentials = $request->only('email', 'password');
        // if (Auth::attempt($credentials)) {
        //     $request->session()->regenerate();

        //     return redirect()->intended('/admin/service');
        // } else {
        //     return view('admin.auth.login');
        // }
    }

    public function logout()
    {
        Auth::guard('web')->logout();
        Session::flush(); // Clear all session data
        // Artisan::call('cash:clear');
        return redirect()->route('admin.loginPage');
    }

    public function register()
    {
        return view('admin.auth.register');
    }
    public function registersend(Request $request)
    {
        // Registration validation
        $request->validate([
            "name" => ['required'],
            "email" => ['required', 'email', 'unique:users'],
            "password" => ['required'],
            "type" => ['required'],
        ]);

        // Hash password
        $password = Hash::make($request->password);

        // Create user
        User::create([
            'type' => $request->type,
            'name' => $request->name,
            'email' => $request->email,
            'password' => $password
        ]);
        // Redirect to login page with success message
        return redirect()->route("admin.loginPage");
    }
}
