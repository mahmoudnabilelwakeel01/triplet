<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Notification;
use App\Models\OfferTourist;
use App\Models\OfferTouristRequest;
// use App\Models\OfferVolunteer;
use App\Models\OfferVolunteerEducational;
use App\Models\OfferVolunteerNoneducational;
use App\Models\OfferVolunteerRequest;
use App\Models\Reservation;
use App\Models\Transportation;
use App\Models\User;
use Illuminate\Support\Facades\Session;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function home()
    {
        return view('home');
    }
    // your destination
    public function bisc()
    {
        return view('bisc');
    }
    public function volunteering()
    {
        return view('volunteering');
    }
    public function liveex()
    {
        return view('liveex');
    }
    public function transportation()
    {
        $transportations = Transportation::get();
        return view('transportation', compact('transportations'));
    }
    // service // reservation
    public function ser_mdn()
    {
        return view('ser_mdn');
    }
    // service // reservation // riyadh
    public function serv_reserve_riyadh()
    {
        $reservations = Reservation::where('city', 1)->get();
        return view('serv_reserve_riyadh', compact('reservations'));;
    }
    // service // reservation // jeddah
    public function serv_reserve_jeddah()
    {
        $reservations = Reservation::where('city', 2)->get();
        return view('serv_reserve_jeddah', compact('reservations'));;
    }
    // service // reservation // makkah
    public function serv_reserve_makkah()
    {
        $reservations = Reservation::where('city', 3)->get();
        return view('serv_reserve_makkah', compact('reservations'));;
    }
    // service // reservation // madenah
    public function serv_reserve_madenah()
    {
        $reservations = Reservation::where('city', 4)->get();
        return view('serv_reserve_madenah', compact('reservations'));;
    }
    // contact page
    public function contact()
    {
        return view('contact');
    }
    // contact page send
    public function contactsend(Request $request)
    {
        Contact::create([
            'fname' => $request->fname,
            'lname' => $request->lname,
            'email' => $request->email,
            'message' => $request->message,
        ]);
        return view('contactsent');
    }
    public function aboutus()
    {
        return view('aboutus');
    }
    //new-notifications
    public function new_notifications()
    {
        $notifications = Notification::where('user_id', auth()->guard('web')->user()->id)->get();
        // dd(auth()->guard('web')->user()->id);
        return view('new_notifications', compact('notifications'));
    }

    // company tourist request
    public function touristreqcompany()
    {
        $offerTouristRequests = OfferTouristRequest::where('status', NULL)->get();
        return view('touristreqcompany', compact('offerTouristRequests'));
    }
    // company tourist request previous
    public function previoustouristreqcompany()
    {
        $offerTouristRequests = OfferTouristRequest::where('status', '!=', NULL)->get();
        return view('previoustouristreqcompany', compact('offerTouristRequests'));
    }

    // resident request-history
    public function bisresident()
    {
        $offerVolunteerRequests = OfferVolunteerRequest::where('status', NULL)->get();
        return view('biscompany', compact('offerVolunteerRequests'));
    }
    // resident request-history previous
    public function previousbisresident()
    {
        $offerVolunteerRequests = OfferVolunteerRequest::where('status', '!=', NULL)->get();
        return view('previousbisresident', compact('offerVolunteerRequests'));
    }
    // resident request-history detail
    public function bisresidentdetail($id)
    {
        $offerVolunteerRequest = OfferVolunteerRequest::find($id);

        return view('biscompanydetail', compact('offerVolunteerRequest'));
    }
    // company request-history  accept
    public function bisresidentaccept($id)
    {
        $offerVolunteerRequest = OfferVolunteerRequest::find($id);
        $offerVolunteerRequest->status = 1;
        $offerVolunteerRequest->save();
        $notification = Notification::where('offer_id', $offerVolunteerRequest->id)->update([
            'status' => 1,
        ]);

        return view('biscompanyaccept');
    }
    // resident request-history  reject
    public function bisresidentreject($id)
    {
        $offerVolunteerRequest = OfferVolunteerRequest::find($id);
        $offerVolunteerRequest->status = 0;
        $offerVolunteerRequest->save();
        $notification = Notification::where('offer_id', $offerVolunteerRequest->id)->update([
            'status' => 0,
        ]);
        return view('biscompanyreject');
    }
    // company request-history  accept
    public function biscompanyaccept($id)
    {
        $offerTouristRequest = OfferTouristRequest::find($id);
        $offerTouristRequest->status = 1;
        $offerTouristRequest->save();
        $notification = Notification::where('offer_id', $offerTouristRequest->id)->update([
            'status' => 1,
        ]);

        return view('bisaccept');
    }
    // company request-history  reject
    public function biscompanyreject($id)
    {
        $offerTouristRequest = OfferTouristRequest::find($id);
        $offerTouristRequest->status = 0;
        $offerTouristRequest->save();
        $notification = Notification::where('offer_id', $offerTouristRequest->id)->update([
            'status' => 0,
        ]);
        return view('bisreject');
    }
    // company previous request
    public function reqcompany()
    {
        $offerVolunteerRequests = OfferVolunteerRequest::get();
        return view('reqcompany', compact('offerVolunteerRequests'));;
    }
    // company previous request delete
    public function reqcompanydelete($id)
    {
        $offerTouristRequest = OfferTouristRequest::find($id);
        $offerTouristRequest->delete();
        return view('reqcompanydelete');
    }

    //  company recebt request
    public function recent_reqcompany()
    {
        return view('recent_reqcompany');
    }

    //request-history
    public function bis()
    {
        $offerVolunteerRequests = OfferVolunteerRequest::where('user_id', Auth()->guard('web')->user()->id)->get();
        return view('bis', compact('offerVolunteerRequests'));
    }
    // previous request
    public function req()
    {
        $offerVolunteerRequests = OfferVolunteerRequest::where('user_id', Auth()->guard('web')->user()->id)->get();
        return view('req', compact('offerVolunteerRequests'));
    }

    // recebt request
    public function recent_req()
    {
        $offerTouristRequests = OfferTouristRequest::where('user_id', Auth()->guard('web')->user()->id)->get();
        return view('recent_req', compact('offerTouristRequests'));
    }
    // recebt request / recent_req_accepted
    public function recent_req_accepted()
    {
        return view('recent_req_accepted');
    }
    // recebt request / recent_req_rejected
    public function recent_req_rejected()
    {
        return view('recent_req_rejected');
    }

    // delete
    public function delete($id)
    {
        $offerVolunteerRequest = OfferVolunteerRequest::find($id);
        if ($offerVolunteerRequest->cv) {
            \Storage::delete($offerVolunteerRequest->cv);
        }
        $offerVolunteerRequest->delete();

        return view('delete');
    }
    // evaluationp
    public function evaluationp()
    {
        return view('evaluationp');
    }
    // evaluation
    public function evaluation()
    {
        return view('evaluation');
    }
    // your-destination/jeddah
    public function jedd()
    {
        return view('jedd');
    }
    //your-destination/ jeddah// best resturant
    public function jeddf()
    {
        return view('jeddf');
    }
    // your-destination/jeddah// places of residence
    public function jeddb()
    {
        return view('jeddb');
    }
    // your-destination/jeddah// best tourist attraction
    public function jeddc()
    {
        return view('jeddc');
    }
    // your-destination/Al-Aula
    public function ula()
    {
        return view('ula');
    }
    // your-destination/Al-Aula // best resturant
    public function ulab()
    {
        return view('ulab');
    }
    // your-destination/Al-Aula // places of residence
    public function ulac()
    {
        return view('ulac');
    }
    // your-destination/Al-Aula // best tourist attraction
    public function ulaf()
    {
        return view('ulaf');
    }

    //your-destination/ mecca
    public function mecca()
    {
        return view('mecca');
    }
    // your-destination/mecca // best resturant
    public function meccab()
    {
        return view('meccab');
    }
    //your-destination/ mecca // places of residence
    public function meccac()
    {
        return view('meccac');
    }
    // your-destination/mecca // best tourist attraction
    public function meccaf()
    {
        return view('meccaf');
    }

    // your-destination/riyadh
    public function riyadh()
    {
        return view('riyadh');
    }
    // your-destination/riyadh // best resturant
    public function riyadhb()
    {
        return view('riyadhb');
    }
    // your-destination/riyadh // places of residence
    public function riyadhc()
    {
        return view('riyadhc');
    }
    // your-destination/riyadh // best tourist attraction
    public function riyadhf()
    {
        return view('riyadhf');
    }

    // your-destination/madinah
    public function madinah()
    {
        return view('madinah');
    }
    // your-destination/madinah // best resturant
    public function madinahb()
    {
        return view('madinahb');
    }
    // your-destination/madinah // places of residence
    public function madinahc()
    {
        return view('madinahc');
    }
    // your-destination/madinah // best tourist attraction
    public function madinahf()
    {
        return view('madinahf');
    }

    // nature and adventure
    public function nature()
    {
        return view('nature');
    }

    // nature and adventure // museums
    public function museums()
    {
        return view('museums');
    }

    // sea and beach
    public function sea()
    {
        return view('sea');
    }
    // sea and beach  // beach
    public function beach()
    {
        return view('beach');
    }

    // Culture and heritageach
    public function culture()
    {
        return view('culture');
    }

    // Culture and heritageach // nature
    public function cultureNature()
    {
        return view('cultureNature');
    }
    // tourist trip
    public function touristtrip()
    {
        return view('touristtrip');
    }
    // tourist trip // Entertainment
    public function trip1()
    {
        $offerTourists = OfferTourist::where('type', 1)->get();
        return view('trip1', compact('offerTourists'));
    }
    // tourist trip // Cultural
    public function trip2()
    {
        $offerTourists = OfferTourist::where('type', 2)->get();
        return view('trip2', compact('offerTourists'));
    }
    // tourist trip // Religious
    public function trip3()
    {
        $offerTourists = OfferTourist::where('type', 3)->get();
        return view('trip3', compact('offerTourists'));
    }
    // tourist trip // Sea
    public function trip4()
    {
        $offerTourists = OfferTourist::where('type', 4)->get();
        return view('trip4', compact('offerTourists'));
    }
    // tourist trip // Environmental
    public function trip5()
    {
        $offerTourists = OfferTourist::where('type', 5)->get();
        return view('trip5', compact('offerTourists'));
    }
    // tourist trip // Mountainous
    public function trip6()
    {
        $offerTourists = OfferTourist::where('type', 6)->get();
        return view('trip6', compact('offerTourists'));
    }
    // tourist trip // triprequest
    public function triprequest($id)
    {
        $offerTourist_id = $id;
        return view('triprequest', compact('offerTourist_id'));
    }
    // tourist trip // triprequest // sent
    public function triprequestsent(Request $request, $id)
    {
        $offer = OfferTouristRequest::create([
            'offerTourist_id' => $id,
            'user_id' => Auth()->guard('web')->user()->id,
            'date' => $request->date,
            'time' => $request->time,
            'notes' => $request->notes,
        ]);
        Notification::create([
            'user_id' => auth()->guard('web')->user()->id,
            'offer_id' => $offer->id,
        ]);
        return view('triprequestsent');
    }
    // VolunteerTrip
    public function volunteertrip()
    {
        return view('volunteertrip');
    }
    // VolunteerTrip  //send
    public function voltrip_send($type, $id)
    {
        $offerVolunteer_id = $id;

        return view('voltrip_send', compact('offerVolunteer_id', 'type'));
    }
    // VolunteerTrip  //sent
    public function voltrip_sent(Request $request, $type, $id)
    {
        $request->validate([
            "name" => ['required'],
            "email" => ['required'],
            "message" => ['required'],
            "describe" => ['required'],
            "cv" => ['required'],
        ]);

        if ($request->hasFile('cv')) {
            $cv = $request->cv->store('/public/offer_volunteer_cv');
            // store logic
            $offer = OfferVolunteerRequest::create([
                'type' => $type,
                'offerVolunteer_id' => $id,
                'user_id' => Auth()->guard('web')->user()->id,
                'name' => $request->name,
                'email' => $request->email,
                'message'   => $request->message,
                'describe'  => $request->describe,
                'cv' => $cv,
            ]);

            Notification::create([
                'user_id' => auth()->guard('web')->user()->id,
                'offer_id' => $offer->id,
            ]);

            return view('offer_tourist_sent');
        } else {
            return redirect()->back();
        }
        // return view('voltrip_sent');
    }
    // VolunteerTrip // education
    public function volunteertripeducation()
    {
        return view('volunteertripeducation');
    }
    // VolunteerTrip // education // english
    public function voltrip_edu_eng()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 1)->get();
        return view('voltrip_edu_eng', compact('offerVolunteers'));
    }
    // VolunteerTrip // education // chinese
    public function voltrip_edu_chin()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 3)->get();
        return view('voltrip_edu_chin', compact('offerVolunteers'));
    }
    // VolunteerTrip // education // french
    public function voltrip_edu_fren()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 2)->get();
        return view('voltrip_edu_fren', compact('offerVolunteers'));
    }
    // VolunteerTrip // education // india
    public function voltrip_edu_india()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 6)->get();
        return view('voltrip_edu_india', compact('offerVolunteers'));
    }
    // VolunteerTrip // education // turkish
    public function voltrip_edu_turk()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 4)->get();
        return view('voltrip_edu_turk', compact('offerVolunteers'));
    }
    // VolunteerTrip // education // russian
    public function voltrip_edu_russ()
    {
        $offerVolunteers = OfferVolunteerEducational::where('type', 1)->where('language', 5)->get();
        return view('voltrip_edu_russ', compact('offerVolunteers'));
    }
    // VolunteerTrip // health
    public function volunteertripheal()
    {
        $offerVolunteers = OfferVolunteerNoneducational::where('type', 2)->get();
        return view('volunteertripheal', compact('offerVolunteers'));
    }
    // VolunteerTrip // other
    public function volunteertripother()
    {
        $offerVolunteers = OfferVolunteerNoneducational::where('type', 3)->get();
        return view('volunteertripother', compact('offerVolunteers'));
    }

    // card
    public function card()
    {
        return view('card');
    }
    // card // sent
    public function cardsent()
    {
        return view('cardsent');
    }

    // offer
    public function offer()
    {
        return view('offer');
    }
    // offer_tourist
    public function offer_tourist()
    {
        $offerTourists = OfferTourist::get();
        return view('offer_tourist', compact('offerTourists'));
    }
    // offer_tourist // add
    public function offer_tourist_add()
    {
        return view('offer_tourist_add');
    }
    // offer_tourist // store
    public function offer_tourist_store(Request $request)
    {
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "time" => ['required'],
            "photo" => ['required'],
            "price" => ['required'],
            "currency" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            $photo = $request->photo->store('/public/offer_tourist');
            // store logic
            OfferTourist::create([
                'user_id' => Auth()->guard('web')->user()->id,
                'type' => $request->type,
                'name' => $request->name,
                'city' => $request->city,
                'time' => $request->time,
                'photo' => $photo,
                'price' => $request->price,
                'currency' => $request->currency,
            ]);
            return view('offer_tourist_sent');
        } else {
            return redirect()->back();
        }
    }
    // offer_tourist // edit
    public function offer_tourist_edit($id)
    {
        $offerTourist = OfferTourist::findOrFail($id);
        return view('offer_tourist_edit', compact('offerTourist'));
    }
    // offer_tourist // update
    public function offer_tourist_update($id, Request $request)
    {
        $offerTourist = OfferTourist::findOrFail($id);
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "time" => ['required'],
            "price" => ['required'],
            "currency" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            if ($offerTourist->photo) {
                \Storage::delete($offerTourist->photo);
            }
            $photo = $request->photo->store('/public/offer_tourist');
        } else {
            $photo = $offerTourist->photo;
        }
        // update logic
        $offerTourist->update([
            'type' => $request->type,
            'name' => $request->name,
            'city' => $request->city,
            'time' => $request->time,
            'photo' => $photo,
            'price' => $request->price,
            'currency' => $request->currency,
        ]);
        return redirect()->route('offer_tourist');
    }
    // offer_tourist // delete
    public function offer_tourist_delete($id)
    {
        $offerTourist = OfferTourist::find($id);
        \Storage::delete($offerTourist->photo);
        $offerTourist->delete();
        return view('offer_tourist_delete');
    }


    // offer_volunteer
    public function offer_volunteer()
    {
        // $offerVolunteers = OfferVolunteer::get();
        $offerVolunteerEducationals = OfferVolunteerEducational::get();
        $offerVolunteerNoneducationals = OfferVolunteerNoneducational::get();

        return view('offer_volunteer', compact('offerVolunteerEducationals', 'offerVolunteerNoneducationals'));
    }
    // offer_volunteer // add
    public function offer_volunteer_add()
    {
        return view('offer_volunteer_add');
    }
    // offer_volunteer // store
    public function offer_volunteer_store(Request $request)
    {
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "time" => ['required'],
            "photo" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            $photo = $request->photo->store('/public/offer_volunteer');
            // store logic
            if ($request->type == 1) {
                OfferVolunteerEducational::create([
                    'type' => 1,
                    'language' => $request->language,
                    'user_id' => Auth()->guard('web')->user()->id,
                    'name' => $request->name,
                    'city' => $request->city,
                    'time' => $request->time,
                    'photo' => $photo,
                    'reward' => $request->reward,

                ]);
            } else {
                OfferVolunteerNoneducational::create([
                    'type' => $request->type,
                    'user_id' => Auth()->guard('web')->user()->id,
                    'name' => $request->name,
                    'city' => $request->city,
                    'time' => $request->time,
                    'photo' => $photo,
                    'reward' => $request->reward,

                ]);
            }
            // OfferVolunteer::create([
            //     'type' => $request->type,
            //     'language' => $request->language,
            //     'user_id' => Auth()->guard('web')->user()->id,
            //     'name' => $request->name,
            //     'city' => $request->city,
            //     'time' => $request->time,
            //     'photo' => $photo,
            //     'reward' => $request->reward,

            // ]);
            return view('offer_volunteer_sent');
        } else {
            return redirect()->back();
        }
    }
    // offer_volunteer // edit
    public function offer_volunteer_edit($type, $id)
    {
        // $offerVolunteer = OfferVolunteer::findOrFail($id);
        if ($type == 1) {
            $offerVolunteer = OfferVolunteerEducational::findOrFail($id);
        } else {
            $offerVolunteer = OfferVolunteerNoneducational::findOrFail($id);
        }
        return view('offer_volunteer_edit', compact('offerVolunteer', 'type'));
    }
    // offer_volunteer // update
    public function offer_volunteer_update($type, $id, Request $request)
    {
        if ($type == 1) {
            $offerVolunteer = OfferVolunteerEducational::findOrFail($id);
        } else {
            $offerVolunteer = OfferVolunteerNoneducational::findOrFail($id);
        }
        // $offerVolunteer = OfferVolunteer::findOrFail($id);
        $request->validate([
            "type" => ['required'],
            "name" => ['required'],
            "city" => ['required'],
            "time" => ['required'],
        ]);

        if ($request->hasFile('photo')) {
            if ($offerVolunteer->photo) {
                \Storage::delete($offerVolunteer->photo);
            }
            $photo = $request->photo->store('/public/offer_volunteer');
        } else {
            $photo = $offerVolunteer->photo;
        }
        // update logic
        if ($type == 1) {
            $offerVolunteer->update([
                'type' => $request->type,
                'language' => $request->language,
                'name' => $request->name,
                'city' => $request->city,
                'time' => $request->time,
                'photo' => $photo,
                'reward' => $request->reward,
            ]);
        } else {
            $offerVolunteer->update([
                'type' => $request->type,
                'name' => $request->name,
                'city' => $request->city,
                'time' => $request->time,
                'photo' => $photo,
                'reward' => $request->reward,
            ]);
        }

        return redirect()->route('offer_volunteer');
    }
    // offer_volunteer // delete
    public function offer_volunteer_delete($type, $id)
    {
        if ($type == 1) {
            $offerVolunteer = OfferVolunteerEducational::findOrFail($id);
        } else {
            $offerVolunteer = OfferVolunteerNoneducational::findOrFail($id);
        }
        \Storage::delete($offerVolunteer->photo);
        $offerVolunteer->delete();
        return view('offer_volunteer_delete');
    }

    // profile
    public function profile()
    {
        $user = Auth()->guard('web')->user();

        return view('profile', compact('user'));
    }
    public function profileSave(Request $request)
    {
        $user=User::where('id',Auth()->guard('web')->user()->id)
        ->update([
            'name' => $request->name,
            'surname'=> $request->surname,
            'mobile'=> $request->mobile,
            'address1'=> $request->address1,
            'address2'=> $request->address2,
            'postcode'=> $request->postcode,
            'state'=> $request->state,
            'area'=> $request->area,
            'country'=> $request->country,
            'stateRegion'=> $request->stateRegion,
            'email'=> $request->email,
        ]);
        Session::flash('success', 'Data saved successfully');
        return redirect()->back();
    }
}
