<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferTourist extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',    // 1 Entertainment - 2 Cultural -  3 Religious - 4 Sea + 5 Environmental - 6 Mountainous
        'user_id',
        'name',
        'city',
        'time',
        'photo',
        'price',
        'currency',
        'status',
    ];

    /**
     * Get all of the comments for the OfferTourist
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offerTouristRequests()
    {
        return $this->hasMany(OfferTouristRequest::class, 'offerTourist_id', 'id');
    }
        /**
     * Get the user that owns the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
