<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferTouristRequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'offerTourist_id',
        'user_id',
        'date',
        'time',
        'notes',
        'status',

    ];

    /**
     * Get the user that owns the OfferTouristRequest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offerTourist()
    {
        return $this->belongsTo(OfferTourist::class, 'offerTourist_id', 'id');
    }
    /**
     * Get the user that owns the OfferTouristRequest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
