<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferVolunteerEducational extends Model
{
    use HasFactory;
    protected $fillable = [
        'type',    // 1 Education
        'language', // 1 English - 2 French -  3 Chinese - 4 Turkish - 5 Russian  - 6 Indian
        'user_id',
        'name',
        'city',
        'time',
        'photo',
        'reward',
    ];

    /**
     * Get all of the comments for the OfferVolunteer
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function offerVolunteerRequests()
    {
        return $this->hasMany(OfferVolunteerRequest::class, 'offerVolunteer_id', 'id');
    }
        /**
     * Get the user that owns the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }


}
