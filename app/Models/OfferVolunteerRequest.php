<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfferVolunteerRequest extends Model
{
    use HasFactory;
    protected $fillable = [
        'type', ///  1 educational - 2 healthy - 3 other
        'offerVolunteer_id',
        'user_id',
        'name',
        'email',
        'message',
        'describe',
        'cv',
        'status',
    ];

    /**
     * Get the user that owns the OfferVolunteerRequest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offerVolunteerEducational()
    {
        return $this->belongsTo(OfferVolunteerEducational::class, 'offerVolunteer_id', 'id');
    }
    /**
     * Get the user that owns the OfferVolunteerRequest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function offerVolunteerNoneducational()
    {
        return $this->belongsTo(OfferVolunteerNoneducational::class, 'offerVolunteer_id', 'id');
    }
        /**
     * Get the user that owns the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
