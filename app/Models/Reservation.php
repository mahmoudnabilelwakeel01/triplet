<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'type',    // 1 hotel - 2 cafe -  3 restuarant
        'name',
        'city',     // 1 Tabuk - 2 Abha - 3 Taif - 4 Mecca  - 5 Jeddah - 6 Riyadh
        'photo',
        'location',
    ];

        /**
     * Get the user that owns the Notification
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
