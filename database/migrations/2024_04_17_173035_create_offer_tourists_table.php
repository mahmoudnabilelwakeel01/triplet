<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('offer_tourists', function (Blueprint $table) {
            $table->id();
            $table->tinyInteger('type')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('name')->nullable();
            $table->string('city')->nullable();
            $table->time('time')->nullable();
            $table->longText('photo')->nullable();
            $table->string('price')->nullable();
            $table->string('currency')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('offer_tourists');
    }
};
