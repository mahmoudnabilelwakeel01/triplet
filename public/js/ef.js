const stars = document.querySelectorAll('#stars .star');
const comment = document.querySelector('#comment');
const submit = document.querySelector('#submit');

let selectedStar = 0;

stars.forEach((star, index) => {
  star.addEventListener('click', () => {
    selectedStar = index + 1;
    updateStars();
  });
});

submit.addEventListener('click', () => {
  const evaluation = {
    rating: selectedStar,
    comment: comment.value
  };
  console.log(evaluation);
});

function updateStars() {
  stars.forEach((star, index) => {
    if (index < selectedStar) {
      star.innerHTML = '★';
    } else {
      star.innerHTML = '☆';
    }
  });
}
