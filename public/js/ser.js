// const wrapper = document.querySelector(".wrapper"),
// SelectBtn = wrapper.querySelector(".select-btn"),
// searchInp = wrapper.querySelector("input"),
// options = wrapper.querySelector(".options");

// let languages=["English","Chinese","French","India","Turkish","Russian"];
// function addLanguage(selectedLanguage){
//     options.innerHTML = "";
//     languages.forEach(language => {
//         let isSelected = language == selectedLanguage ? "selected" : "";
//         let li= `<li onclick="updateName(this)" class="${isSelected}"> ${language} </li>`;
//         options.insertAdjacentHTML("beforeend",li);
//     });
// }
// addLanguage();
// function updateName(selectedLi){
//     searchInp.value ="";
//     addLanguage(selectedLi.innerText);
//     wrapper.classList.remove("active");
//     SelectBtn.firstElementChild.innerText = selectedLi.innerText;
// }
// searchInp.addEventListener("keyup",() =>{
//     let arr=[];
//     let searchedVal = searchInp.value.toLowerCase();
//     arr = languages.filter(data =>{
//         return data.toLowerCase().startsWith(searchedVal);
//     }) .map(data =>`<li onclick="updateName(this)">${data}</li>` ).join("");
//     options.innerHTML = arr ? arr : `<p> Oops! The options not found </p>`;

// });
// SelectBtn.addEventListener("click", () =>{
//     wrapper.classList.toggle("active");
// });


// Get the wrapper, select button, search input, and options list
const wrapper = document.querySelector('.wrapper');
const selectBtn = document.querySelector('.select-btn');
const searchInput = document.querySelector('.search input');
const optionsList = document.querySelector('.options');

// Array of languages
const languages = ["English", "French", "Chinese", "Turkish", "Russian", "India"];

// Function to add language options
function addLanguage(selectedLanguage) {
    optionsList.innerHTML = '';
    languages.forEach(language => {
        const isSelected = language === selectedLanguage ? 'selected' : '';
        const li = `<li class="${isSelected}" onclick="updateLanguage('${language}')">${language}</li>`;
        optionsList.insertAdjacentHTML('beforeend', li);
    });
}

// Function to update language
function updateLanguage(language) {
    // Clear search input
    searchInput.value = '';

    // Update language in select button
    selectBtn.firstElementChild.textContent = language;

    // Remove 'active' class from wrapper
    wrapper.classList.remove('active');

    // Hide all cards
    document.querySelectorAll('.card').forEach(card => {
        card.style.display = 'none';
    });

    // Show cards with selected language class
    document.querySelectorAll(`.card.${language}`).forEach(card => {
        card.style.display = 'block';
    });
}

// Event listener for search input
searchInput.addEventListener('keyup', () => {
    const searchedValue = searchInput.value.toLowerCase();

    // Filter languages based on searched value
    const filteredLanguages = languages.filter(language => language.toLowerCase().startsWith(searchedValue));

    // Generate list items for filtered languages
    const listItems = filteredLanguages.map(language => `<li onclick="updateLanguage('${language}')">${language}</li>`).join('');

    // Display filtered language options
    optionsList.innerHTML = listItems || '<p>Oops! No options found.</p>';
});

// Event listener for select button
selectBtn.addEventListener('click', () => {
    wrapper.classList.toggle('active');
});

// Initialize language options
addLanguage(languages[0]);
