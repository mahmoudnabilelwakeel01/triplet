<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Service</title>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/aboutas.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
</head>

<body>

    <style>
        html,
        body {
            margin: 0;
            padding: 0;
            height: 100%;
        }

        .container {
            height: 100vh;
            /* اجعل ارتفاع العناصر داخل .container يساوي ارتفاع الشاشة بالكامل */
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    </style>

    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        {{-- <a href="{{ route('biscompany') }}" id="requests-history">Volenteer request </a> --}}

                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="#">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>

    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <div
        style="width:100%;
           margin-top: 10%;
           height:1%;
           display: flex;
           justify-content: center;
           align-items: center;
           background-color: white;">
        <br>


        <img src="{{ asset('public/img/photo_2024-03-21_01-43-53.jpg') }}"
            style="height: 130px;
            width:145px;
            border-radius: 50%;
            border:10px solid #135313">
    </div>
    <div style="text-align: center;">
        <div style="margin-top: 95px;">
            <p style="color: #135313; font-family: 'Times New Roman';font-size: 30px;"> Wheare are you ?</p>
            <p style="color: #135313; font-family: 'Times New Roman';font-size: 20px;"> Volunteer tourism site to
                find
                additional experiences and adventures</p>
        </div>
        <br>
        <br>
        <br>

    </div>



    <div class="container" style="margin-top:-12%;">
        <div class="card">
            <div class="box">
                <div class="content">
                    <h2 style="right: 30%;">Our goal</h2>
                    <p>We provide our services professionally in line with the goals of the
                        Kingdom of Saudi Arabia's vision by improving our customer services
                        and our company's performance at the level of the Kingdom's great
                        achievements in opening up to countries of the world in all fields,
                        especially in the field of tourism and volunteering.</p>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="box">
                <div class="content">
                    <h2>Our message </h2>
                    <br>
                    <p>Traveling and volunteering, inspiring and acquiring new cultures.
                        We strive to achieve excellence for our valued customers by providing the
                        best services, developing them and managing them professionally.
                        according to the best international standards at competitive prices with
                        a high level of quality and innovation to meet their needs and
                        aspirations that guarantee their satisfaction</p>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="box">
                <div class="content">
                    <h2>Our vision</h2>
                    <br>
                    <p>Reaching a prominent position in the Kingdom of Saudi Arabia and
                        abroad in the field of tourism, volunteer work, shipping, and related
                        services through the formation of local and international partnerships,
                        to develop our services and moovate methods of providing them that
                        parallel the development of the tourism field and the field of volunteer
                        work at the internal and international levels.</p>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
</body>

</html>
