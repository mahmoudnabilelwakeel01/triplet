<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8" />
    <!--<title> Login and Registration Form in HTML & CSS | CodingLab </title>-->
    <link rel="stylesheet" href="{{ asset('public/css/login.css') }}" />
    <!-- Fontawesome CDN Link -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <body>
        <div class="container">
            <input type="checkbox" id="flip" />
            <div class="cover">
                <div class="front">
                    <img src="{{ asset('public/img/logo2.jpg') }}" alt="" />
                    <div class="text">
                        <span class="text-1"> </span>
                        <span class="text-2"> </span>
                    </div>
                </div>
                <div class="back">
                    <img class="backImg" src="{{ asset('public/img/logo2.jpg') }}" alt="" />
                    <div class="text">
                        <span class="text-1"> </span>
                        <span class="text-2"> </span>
                    </div>
                </div>
            </div>
            <div class="forms">
                <div class="form-content">
                    <div class="login-form">
                        <div class="title">Login</div>
                        <form action="{{ route('admin.loginattemp') }}" method="post">
                            @csrf
                            <div class="input-boxes">
                                <div class="input-box">
                                    <i class="fas fa-envelope"></i>
                                    <input type="text" placeholder="Enter your email" name="email" required />
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-lock"></i>
                                    <input type="password" placeholder="Enter your password" name="password" required />
                                </div>
                                <div class="button input-box">
                                    <input type="submit" value="Sumbit" />
                                </div>
                                <div class="text sign-up-text">
                                    Don't have an account? <label for="flip">Sign Up now</label>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="signup-form">
                        <div class="title">Signup</div>
                        <form action="{{ route('admin.registersend') }}" method="post">
                            @csrf
                            <div class="input-boxes">
                                <div class="input-box">
                                    <i class="fas fa-user"></i>
                                    <input type="text" placeholder="Enter your name" name="name" required />
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-envelope"></i>
                                    <input type="email" placeholder="Enter your email" name="email" required />
                                </div>
                                <div class="input-box">
                                    <i class="fas fa-lock"></i>
                                    <input type="password" placeholder="Enter your password" name="password" required />
                                </div>
                                <br />

                                <div class="tt">
                                    <input type="radio" id="tourist" name="type" value="2" />
                                    <label for="tourist"> Tourist </label> <br />
                                    <input type="radio" id="beneficiary" name="type" value="4" />
                                    <label for="beneficiary"> Beneficiary </label><br />
                                    <input type="radio" id="company" name="type" value="3" />
                                    <label for="company"> Company </label>
                                </div>

                                <div class="button input-box">
                                    <input type="submit" value="Sumbit" />
                                </div>
                                <div class="text sign-up-text">
                                    Already have an account? <label for="flip">Login now</label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

</html>
