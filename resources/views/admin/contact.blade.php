<!DOCTYPE html>
<html>

<head>
    <title></title>
    <style>
        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 20px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }


        a {

            color: #135313;
        }

        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }
    </style>
    <link rel="stylesheet" href="{{ asset('public/admin/css/contact_select.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
</head>

<body>
    <nav>

        <div class="logo"><a href="#"><img src="{{ asset('public/img/logot.jpg') }}" alt="TripleT" width="60px"
                    height="60px"></a></div>


        <ul>

            <div class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{ route('admin.service') }}">Service Management</a></li>
                <li><a href="{{ route('admin.contact') }}">Massages</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#"><ion-icon name="notifications-outline"></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li><a href="{{route('profile')}}"><ion-icon name="person-circle-outline"></ion-icon></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li>
                    <a href="{{ route('admin.logout') }}">
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </li>

            </div>
        </ul>

    </nav>

    <div class="filter">
    </div>
    <table>
        <tr>
            <th colspan="7"style="background-color: #135313;">
                <h4 style="margin-right: 70%; color: white; font-family: 'Times new';">All Contact Messages</h4>
            </th>
        </tr>
        <tr>
            <td>#</td>
            <td>Date</td>
            <td> Name </td>
            <td>E-mail </td>
            <td>Message</td>
            <td>Response</td>
            <td>Action</td>
        </tr>
        @foreach ($contacts as $contact)
            <tr>
                <td>{{$contact->id ?? ''}}</td>
                <td>{{ $contact->created_at->format('Y-m-d') ?? ''}}</td>
                <td>{{$contact->fname ?? ''}}  {{$contact->lname ?? ''}}</td>
                <td>{{$contact->email ?? ''}}</td>
                <td>{{$contact->message ?? ''}}</td>
                <td>{{$contact->respnose ?? ''}}</td>
                <td>
                    @if ($contact->respnose === NULL)
                    <a href="{{route('admin.contactresponse',$contact->id)}}" style="padding: 10px"><i class="fa-solid fa-pen" style="color: #263e0f;"></i>
                    @endif
                    <a href="{{route('admin.contactdelete',$contact->id)}}"><i class="fa-solid fa-trash" style="color: #263e0f;"></i>
                </td>
            </tr>
        @endforeach
    </table>

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
</body>

</html>
