<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Response
    </title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 'Times New Roman';
            box-sizing: border-box;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            margin: 0;
            padding: 0;


        }

        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 20px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }


        a {

            color: #135313;
        }

        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        body {

            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
            background-color: #fff;
        }

        .message {

            width: 500px;
            height: 70px;
            border: 1px solid black;
            padding-left: 145px;
            background-color: #0d520f;
            color: rgb(255, 255, 255);
            margin-top: 150px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 30px;
            line-height: 70px;
            margin-left: 440px;
            text-align: left;
            border-radius: 8px;


        }

        table {
            width: 40%;
            border-collapse: collapse;
            border-radius: 10px;
            overflow: scroll;
            margin: 20px auto;
            border-collapse: collapse;
            border: 2px solid #1f4b2e;
            margin-left: 430px;
            border-style: double;


            /* اللون الأخضر */
        }

        th,
        td {
            padding: 15px;
            text-align: left;
            border-bottom: 0.5px solid #e8ece8;

            /* اللون الأبيض */
        }

        td {
            height: 50px;
            text-align: center;

        }

        select {
            height: 30px;
            width: 170px;
            justify-content: center;
            align-items: center;
            font-family: 'Times New Roman';
            text-align: center;
            font-size: 15px;
            color: #0c4e09;


        }

        input {
            size: 50px;
            width: 170px;
            height: 30px;
            font-size: 15px;
            color: #0c4e09;

        }

        th {
            background-color: #ffffff;
            color: #11710e;
            font-family: 'Times New Roman';
            font-size: 20px;
            /* لون النص أبيض */
        }

        tr:hover {
            background-color: #f8f8f8;
            color: #f4f4f4;
            font-family: 'Times New Roman';
            font-size: 20px;
            /* لون الخلفية الفاتح عند التحويم */
        }

        input[type="submit"] {
            background-color: #0d5f2f;
            color: #fff;
            size: 40px;
            padding: 10px 40px;
            border: none;
            cursor: pointer;
            border-radius: 10px;
            height: 30px;

        }

        input[type="time"] {
            width: 170px;
            height: 30px;
        }

        input[type="file"] {
            border: 2px #124f0a;
            border-color: #0d520f;
            width: 200px;
            padding: 5px;

        }

        button {



            font-family: 'Times New Roman';


            background-color: #008000;
            color: #fff;
            margin-right: 10px;
            font-size: 20px;
            width: 200px;
            height: 50px;
            background-color: rgb(14, 83, 25);
            color: white;
            border: none;
            border-radius: 10px;
            box-shadow: 2px 2px 4px rgba(255, 255, 255, 0.3);
            text-shadow: 1px 1px 1px rgba(7, 69, 41, 0.5);
            padding: 10px;
            margin-left: 590px;
            border-style: double;
        }

        .right-buttom {
            float: right;
        }

        button:hover {
            background-color: #177a21;
        }


        .btn-success {
            background-color: #0d5f2f;
            color: white;
            border: none;
            border-radius: 5px;
            padding: 8px 20px;
            cursor: pointer;
            transition: background-color 0.3s;
        }

        .btn-success:hover {
            background-color: #008000;
        }

        /* Adjusting the width of the file input */
        input[type="file"] {
            width: calc(100% - 100px);
            /* Subtracting the width of the button */
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>


</head>

<body>
    <nav>

        <div class="logo"><a href="#"><img src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"
                    width="60px" height="60px"></a></div>


        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('admin.service') }}">Service Management</a></li>
                <li><a href="{{ route('admin.contact') }}">Massages</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#"><ion-icon name="notifications-outline"></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li><a href="{{ route('profile') }}"><ion-icon name="person-circle-outline"></ion-icon></ion-icon
                            width="2000px" height="2000px"></a></li>
                <li>
                    <a href="{{ route('admin.logout') }}">
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </li>

            </div>
        </ul>

    </nav>


    <br>
    <div class="header"></div>
    <div class="message">response to message </div>
    <div class="products-container">
        <div class="product" data-name="p-1">
            <form action="{{ route('admin.contactresponsesave',$contact->id) }}" method="post" >
                @csrf
                <table>
                    <tr>
                        <th>Date *</th>
                        <td>{{ $contact->created_at->format('Y-m-d') ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Name *</th>
                        <td>{{ $contact->fname ?? '' }} {{ $contact->lname ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Email *</th>
                        <td>{{ $contact->email ?? '' }}</td>
                    </tr>
                    <tr>
                        <th>Message *</th>
                        <td>{{ $contact->message ?? '' }}</td>
                    </tr>

                    <tr>
                        <th>Response*</th>
                        <td><input type="text" name="respnose" required></td>
                    </tr>
                </table>
                <button type="submit">Send</button>
            </form>
        </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br><br>

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>

</body>

</html>
