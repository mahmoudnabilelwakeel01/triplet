<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Current places</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 'Times New Roman';
            box-sizing: border-box;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            margin: 0;
            padding: 0;


        }

        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 30px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }



        a {

            color: #135313;
        }

        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        * {
            margin: 0;
            padding: 0;
            font-family: 'Times New Roman';
            box-sizing: border-box;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            margin: 0;



        }

        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 20px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }


        .fa-plus {
            font-size: 30px;
            margin: 0 10px;
            color: rgb(7, 80, 7);
            margin-left: 300px;
        }

        .fa-user {
            font-size: 25px;
        }

        .fa-bell {
            font-size: 25px;
        }

        .line {
            border-bottom: 2px solid green;
            /* سمك ولون الخط */
            width: 55%;
            /* عرض الخط (يمكن تعديله حسب الحاجة) */
            margin-top: 12px;
            position: fixed;
            margin-left: 300px;
        }

        table {
            border-collapse: collapse;
            width: 50%;
            /* عرض الجدول (يمكن تعديله حسب الحاجة) */
            margin: 20px;
            /* المسافة من الأطراف */
            position: fixed;
            margin-left: 300px;

        }

        th,
        td {
            padding: 15px;
            text-align: center;
        }

        th {
            background-color: rgb(7, 80, 7);
            /* لون الخلفية للعناصر العنوانية (Header) */
            color: white;
            border-color: beige;
            /* لون النص للعناصر العنوانية */
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
            border-color: rgb(193, 193, 179);
            /* لون الخلفية للصفوف الزوجية */
        }

        .fa-trash {
            color: rgb(7, 80, 7);
        }

        .fa-pencil {
            color: rgb(7, 80, 7);

        }

        .fa-plus {
            font-size: 30px;
            margin: 0 10px;
            color: rgb(7, 80, 7);
            margin-left: 350px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>

</head>

<body>
    <nav>

        <div class="logo"><a href="#"><img src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"
                    width="60px" height="60px"></a></div>


        <ul>

            <div class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{ route('admin.service') }}">Service Management</a></li>
                <li><a href="{{ route('admin.contact') }}">Massages</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#"><ion-icon name="notifications-outline"></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li><a href="{{route('profile')}}"><ion-icon name="person-circle-outline"></ion-icon></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li>
                    <a href="{{ route('admin.logout') }}">
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </li>

            </div>
        </ul>

    </nav>


    <br>
    <br>
    <br>
    <br>
    <br><br><br><br><br><br><br><br>
    <p style="color: #065906; font-size: 30px; text-align: center; color: hsl(120, 90%, 16%); font-size: 40px;"> Current
        places
    </p>

    </head>

    <body>
        <a href="{{ route('admin.serv_reservation_add') }}"> <i class="fa-solid fa-plus"></i></a>
        </head>

        <body>
            <div class="line"></div>
            <br><br>
            <table>
                <thead>
                    <tr>
                        <th>Type</th>
                        <th>Name</th>
                        <th>City</th>
                        <th>Photo</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <!-- يمكنك إضافة صفوف إضافية حسب الحاجة -->
                    @foreach ($reservations as $reservation)
                        <tr>
                            <td>
                                @switch($reservation->type)
                                    @case(1)
                                        Hotel
                                    @break

                                    @case(2)
                                        Cafe
                                    @break

                                    @case(3)
                                        Restuarant
                                    @break

                                    @default
                                @endswitch
                            </td>
                            <td>{{ $reservation->name ?? '' }}</td>
                            <td>
                                @switch($reservation->city)
                                    @case(1)
                                        Riyadh
                                    @break

                                    @case(2)
                                        Jeddah
                                    @break

                                    @case(3)
                                        Mecca
                                    @break

                                    @case(4)
                                        Madenah
                                    @break

                                    @default
                                @endswitch

                            </td>
                            <td>
                                <img class="logo-abbr" style="width:60px "
                                    @if ($reservation->photo) src="{{ asset('public/' . Storage::url($reservation->photo)) }}" @endif>

                            </td>
                            <td>{{ $reservation->location ?? '' }}</td>

                            <td><a href="{{ route('admin.serv_reservation_edit', $reservation->id) }}"><i
                                        class="fa-solid fa-pencil"></i></a>
                                <a href="{{ route('admin.serv_reservation_delete', $reservation->id) }}"><i
                                        class="fa-solid fa-trash" style="color: #263e0f;"></i>
                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>


            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br><br> <br>

        </body>

        <footer>
            <div class="footer-icons">
                <a href="#"><i class="fa-brands fa-twitter"></i></a>
                <a href="#"><i class="fa-brands fa-tiktok"></i></a>
                <a href="#"><i class="fa-brands fa-instagram"></i></a>
            </div>
        </footer>

</html>
