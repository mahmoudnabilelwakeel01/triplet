<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Message Delete
    </title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 'Times New Roman';
            box-sizing: border-box;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            margin: 0;
            padding: 0;


        }

        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 20px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }


        a {

            color: #135313;
        }

        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        .menu {
            display: flex;
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-top: 95px;
        }

        /* أضف هذا الكود لتحديد لون زر "Find out more" */

        .p {
            color: #065906;
            font-size: 30px;
            text-align: center;
            color: hsl(120, 90%, 16%);
            font-size: 40px;
        }

        .notification-box {
            position: fixed;
            z-index: 99;
            top: 28px;
            right: 190px;
            width: 50px;
            height: 50px;
            text-align: center;
        }

        .notification-bell {
            animation: bell 1s 1s both infinite;
        }

        .notification-bell * {
            display: block;
            margin: 0 auto;
            background-color: #0c522a;
            box-shadow: 0px 0px 15px #fff;
        }

        .bell-top {
            width: 6px;
            height: 6px;
            border-radius: 3px 3px 0 0;
        }

        .bell-middle {
            width: 25px;
            height: 25px;
            margin-top: -1px;
            border-radius: 12.5px 12.5px 0 0;
        }

        .bell-bottom {
            position: relative;
            z-index: 0;
            width: 32px;
            height: 2px;
        }

        .bell-bottom::before,
        .bell-bottom::after {
            content: '';
            position: absolute;
            top: -4px;
        }

        .bell-bottom::before {
            left: 1px;
            border-bottom: 4px solid #fff;
            border-right: 0 solid transparent;
            border-left: 4px solid transparent;
        }

        .bell-bottom::after {
            right: 1px;
            border-bottom: 4px solid #fff;
            border-right: 4px solid transparent;
            border-left: 0 solid transparent;
        }

        .bell-rad {
            width: 8px;
            height: 4px;
            margin-top: 2px;
            border-radius: 0 0 4px 4px;
            animation: rad 1s 2s both infinite;
        }

        .notification-count {
            position: absolute;
            z-index: 1;
            top: -6px;
            right: -6px;
            width: 30px;
            height: 30px;
            line-height: 30px;
            font-size: 18px;
            border-radius: 50%;
            background-color: #ff4927;
            color: #fff;
            animation: zoom 3s 3s both infinite;
        }

        @keyframes bell {
            0% {
                transform: rotate(0);
            }

            10% {
                transform: rotate(30deg);
            }

            20% {
                transform: rotate(0);
            }

            80% {
                transform: rotate(0);
            }

            90% {
                transform: rotate(-30deg);
            }

            100% {
                transform: rotate(0);
            }
        }

        @keyframes rad {
            0% {
                transform: translateX(0);
            }

            10% {
                transform: translateX(6px);
            }

            20% {
                transform: translateX(0);
            }

            80% {
                transform: translateX(0);
            }

            90% {
                transform: translateX(-6px);
            }

            100% {
                transform: translateX(0);
            }
        }

        @keyframes zoom {
            0% {
                opacity: 0;
                transform: scale(0);
            }

            10% {
                opacity: 1;
                transform: scale(1);
            }

            50% {
                opacity: 1;
            }

            51% {
                opacity: 0;
            }

            100% {
                opacity: 0;
            }
        }

        @keyframes moon-moving {
            0% {
                transform: translate(-200%, 600%);
            }

            100% {
                transform: translate(800%, -200%);
            }
        }


        a {
            color: #135313;
        }

        .header {
            background-color: white;
        }

        .message {
            width: 1340px;
            height: 70px;
            border: 1px solid black;
            padding-left: 200px;
            background-color: #154b158a;
            color: black;
            margin-top: 150px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 30px;
            line-height: 70px;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>


</head>

<body>
    <nav>

        <div class="logo"><a href="#"><img src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"
                    width="60px" height="60px"></a></div>


        <ul>

            <div class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{ route('admin.service') }}">Service Management</a></li>
                <li><a href="{{ route('admin.contact') }}">Massages</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#"><ion-icon name="notifications-outline"></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li><a href="{{route('profile')}}"><ion-icon name="person-circle-outline"></ion-icon></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li>
                    <a href="{{ route('admin.logout') }}" >
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </li>

            </div>
        </ul>

    </nav>


    <div class="main"> </div>

    <div class="header"> </div>

    <div class="message">


        The Transportation was delete successfully </div>

</body>
<footer>
    <div class="footer-icons">
        <a href="#"><i class="fa-brands fa-twitter"></i></a>
        <a href="#"><i class="fa-brands fa-tiktok"></i></a>
        <a href="#"><i class="fa-brands fa-instagram"></i></a>
    </div>
</footer>

</html>
