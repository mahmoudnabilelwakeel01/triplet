<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Service Management</title>
    <style>
        * {
            margin: 0;
            padding: 0;
            font-family: 'Times New Roman';
            box-sizing: border-box;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            margin: 0;
            padding: 0;


        }

        nav {
            position: fixed;
            top: 0;
            left: 0;
            width: 105%;
            height: 80PX;
            padding: 0.5px 0.5px;
            box-sizing: border-box;
            background: rgba(rgb);
            border-bottom: 1px solid #135313;
            display: flex;

        }

        nav .logo {
            padding: 2px 2px;
            height: 80px;
            float: left;
            font-size: 15px;
            font-weight: bold;
            text-transform: none;
            color: #135313;

        }

        nav ul .menu {
            list-style: none;
            float: right;
            margin: 0;
            padding: 0;
            display: flex;

        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            padding: 40px 20px;
            margin-left: 65px;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;


        }

        nav ul li a:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
            border-radius: 5px;
        }



        a {

            color: #135313;
        }

        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        body {

            align-items: center;
            justify-content: center;
            height: 100vh;
            margin: 0;
            background-color: #fff;
        }

        .card {
            width: 320px;
            height: 420px;
            border-radius: 20px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transform: translateY(0);
            transition: transform 0.3s ease;
            margin: 30px;
        }

        .card:hover {
            transform: translateY(-10px);
        }

        .card img {
            width: 300px;
            height: 250px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-content {
            padding: 20px;
            height: 500px;
            background-color: #fff;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .card-content h2 {
            margin-top: 0;
            margin-left: 70px;
            color: #135313;
        }

        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #135313;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-left: 85px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
        }

        .ss {
            display: flex;
            border-radius: 15px;
            justify-content: center;
        }

        .aa {
            display: flex;
            justify-content: center;
        }
    </style>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>


</head>

<body>
    <nav>

        <div class="logo"><a href="#"><img src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"
                    width="60px" height="60px"></a></div>


        <ul>

            <div class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{ route('admin.service') }}">Service Management</a></li>
                <li><a href="{{ route('admin.contact') }}">Massages</a></li>
                <li><a href="#">About Us</a></li>
                <li><a href="#"><ion-icon name="notifications-outline"></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li><a href="{{route('profile')}}"><ion-icon name="person-circle-outline"></ion-icon></ion-icon width="2000px"
                            height="2000px"></a></li>
                <li>
                    <a href="{{ route('admin.logout') }}" >
                        <ion-icon name="log-out-outline"></ion-icon>
                    </a>
                </li>

            </div>
        </ul>

    </nav>

    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="aa">
        <div class="card">
            <img src="{{ asset('public/admin/img/trans.png') }}" alt="Card Image" height="200" width="400">
            <div class="card-content">
                <br>
                <h2>Transportation</h2>
                <br>

                <br>
                <a href="{{ route('admin.serv_transportation') }}" class="btn">View Now</a>
            </div>
        </div>
        <div class="card">
            <img src="{{ asset('public/admin/img/pla.png') }}" alt="Card Image" height="200" width="400">
            <div class="card-content">
                <br>
                <h2>Reservation</h2>
                <br>

                <br>
                <a href="{{ route('admin.serv_reservation') }}" class="btn">View Now</a>
            </div>
        </div>
    </div>

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>

</body>

</html>
