<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/c.css') }}">
    <!-- Swiper CSS -->
    <link rel="stylesheet" href="{{ asset('public/css/swiper-bundle.min.css') }}">

    <!-- CSS -->

    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    {{-- <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}"> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Contact</title>
</head>





<body>

    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{route('home')}}">Home</a></li>
                <li><a href="{{route('bisc')}}">Your destination</a></li>
                <li><a href="{{route('volunteering')}}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{route('liveex')}}">Live Experience</a></li>
                <li><a href="#">Service</a></li>
                {{-- <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{route('transportation')}}"  class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{route('ser_mdn')}}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li> --}}
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{route('aboutus')}}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{route('new_notifications')}}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br><br><br><br><br><br><br>
    <h2>
        <p style="margin-left:44%;color: green;font-family: Times New Roman;font-size: 25px;" class="mt-1">Get in
            Touch</p>
    </h2>
    <img src="{{ asset('public/img/logo1.jpg')}}" class="img1" style=" height: 30%; width: 20%;">
    <div class="for">
        <form  action="{{route('contactsend')}}" method="post">
            @csrf
            <span>
                <div style="margin-right: 15px;">
                    <label for="fname" style="font-family: Times New Roman;">First name:</label><br>
                    <input class="bor" type="text" name="fname">
                </div>
                <div style="margin-left: 15px;">
                    <label for="lname" style="font-family: Times New Roman;">Last name:</label><br>
                    <input type="text" style="border-radius: 0%; border-color: green; " name="lname">
                </div>
            </span>
            <br>
            <div class="div"> <label class="" for="fname">E-mail:</label><br>
                <input type="email" style="border-radius: 0%; border-color: green;  " name="email">
            </div>

            <div style="padding-right: 50px;" class="div">
                <label for="fname">Message:</label><br>
                <input type="text" style="border-radius: 0%; border-color: green; height: 90px; width: 115%;"
                    name="message">
            </div>
            <div style="text-align: center;">
                <div dir="rtl" class="div">
                    <button class="but"  type="submit" center;>send</button>
                </div>
            </div>
        </form>
    </div>



    <br>  <br>    <br>    <br>    <br>
    <br>  <br>    <br>    <br>    <br>
    <footer class="footer">
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
</body>
<script src="{{ asset('public/js/swiper-bundle.min.js') }}"></script>


<script>
    function send() {
        window.alert("Send Succesfully")
    }
</script>
</html>
