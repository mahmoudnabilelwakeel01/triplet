<html>

<head>

    <meta charset="UTF-8">
    <title>TripleT</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/museums.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        custom-btn {
            background-color: #135313;
            /* لون أخضر داكن */
            color: #ffffff;
            /* لون أبيض للنص */
        }

        .custom-btn:active {
            background-color: #ffffff;
            /* لون الخلفية الأبيض */
            color: #135313;
            /* لون النص الأخضر */
        }

        body,
        html {
            height: 100vh;
            margin: 0;
            padding: 0;
        }

        .slick-custom {
            height: 100vh;
            /* أو أي ارتفاع ترغب فيه */
            margin: 0;
            /* إزالة الهوامش */
            padding: 0;
            /* إزالة الحواف الداخلية */

        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            padding: 2px;
            border-radius: 20px;
            /* زيادة قيمة border-radius لجعل الحواف أكثر دائرية */
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            font-size: 14px;
            /* حجم النص */
            color: black;
            padding: 6px 16px;
            /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li><a href="#">Service</a></li>
                {{-- <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li> --}}
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{route('new_notifications')}}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>

    <br> <br> <br> <br> <br> <br>



    <div class="container">
        <div class="card">
            <a href="https://maps.app.goo.gl/gRSPkxhirDcn39cc7?g_st=ic"><img
                    src="{{ asset('public/ing/Nature1.jpg') }}" alt="charset"></a>
            <div class="intro">
                <h1>Diriyah</h1><br>
                <p>
                <div class="fa-solid fa-location-dot"></div><br><br></p> <span> Diriyah is a historical treasure rich in
                    culture, beauty,
                    nature and art, and is an exceptional world destination for history and culture lovers.</span> <br>
                </p>


            </div>
        </div>
        <div class="card">
            <a href="https://maps.app.goo.gl/XVtwzbLrya6wMdLK7?g_st=ic
           "><img
                    src="{{ asset('public/ing/Nature4.jpg') }}" alt="charset"></a>
            <div class="intro">
                <h1>
                    Al Waaba Crater</h1>
                <br>
                <p>
                <div class="fa-solid fa-location-dot"></div> <br> <br>It is the largest of its kind in the Middle East.
                </p> <span></span></p>

            </div>
        </div>
        <div class="card">
            <a href="https://maps.app.goo.gl/VW8gWi9KeHmVW8aw6?g_st=ic"><img
                    src="{{ asset('public/ing/Nature2.jpg') }}" alt="charset"></a>
            <div class="intro">
                <h1> Valley of the Moon</h1><br>
                <p>
                <div class="fa-solid fa-location-dot"></div><br><br><br> It was given this name due to the appearance of
                the rocks and terrain that resemble the shape of the moon. </p> <span> </span>
                </p>


            </div>
        </div>
    </div>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>


</body>

</html>
