<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TripleT</title>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

</head>
<style>
    .fa-user {
        font-size: 25px;
    }

    .fa-bell {
        font-size: 25px;
    }

    .p {
        color: #065906;
        font-size: 30px;
        text-align: center;
        color: hsl(120, 90%, 16%);
        font-size: 40px;
    }

    #container {
        position: relative;
    }

    #img1 {
        top: 0;
        left: 0;
        width: 1500px;
        high: 980px;
    }

    #img2 {
        top: 0;
        left: 0;
        width: 1500px;
        high: 980px;
    }

    #img3 {
        top: 0;
        left: 0;
        width: 1500px;
        high: 980px;
    }

    #img4 {
        top: 0;
        left: 0;
        width: 1500px;
        high: 980px;
    }

    .overlay-text {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
        background-color: rgba(0, 0, 0, 0.7);
        /* خلفية سوداء بنسبة شفافية 70% */
        padding: 20px 40px;
        /* زيادة العرض إلى اليمين واليسار */
        color: #fff;
        /* لون النص أبيض */
        font-size: 20px;
        /* حجم الخط */
        text-align: center;
        max-width: 80%;
        z-index: 2;
        /* للتأكد من عرض النص فوق الصورة */
        display: flex;
        /* تحويل العنصر إلى عنصر مرن */
        align-items: center;
        /* وضع العناصر في المحور الرأسي في الوسط */
        justify-content: center;
        /* وضع العناصر في المحور الأفقي في الوسط */
    }

    .button-link {
        color: #fff;
        /* النص بالأسود افتراضيًا */
        background-color: #fff;
        /* الخلفية بالأبيض افتراضيًا */
        padding: 10px 20px;
        /* تباعد داخلي */
        border: 1px solid #135313;
        /* حدود سوداء */
        text-decoration: none;
        /* إزالة تزيين الرابط */
        background-clip: text;
        /* تمتد الخلفية داخل النص */
    }

    .button-link:hover,
    .button-link:focus {
        /* عند النقر */
        color: #fff;
        /* النص بالأبيض */
        background-color: #fff;
        /* الخلفية بالأخضر */

    }

    .h {
        font-family: 'Times New Roman';
    }
</style>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>
                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset

                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br><br><br><br>


    <div>
        <p>


        </p>

    </div>
    </div>
    <br><br>
    <p style="color: #065906; font-size: 30px; text-align: center; color: hsl(120, 90%, 16%); font-size: 40px;"> Welcome
        in Saudi Arabia
    </p>

    <br><br>
    </div>
    <div style="display: flex; justify-content: center; align-items: center; height: 30vh;">
        <img src="{{ asset('public/ing/t1.jpg') }}" alt="Image 1"
            style="width: 250px; height: 250px; margin: 0 10px;">
        <img src="{{ asset('public/ing/t2.jpg') }}" alt="Image 2"
            style="width: 250px; height: 250px; margin: 0 10px;">
        <img src="{{ asset('public/ing/t3.jpg') }}" alt="Image 3"
            style="width: 250px; height: 250px; margin: 0 10px;">
        <img src="{{ asset('public/ing/t4.jpg') }}" alt="Image 4"
            style="width: 250px; height: 250px; margin: 0 10px;">
    </div>
    <br><br>
    <br>
    <div>
        <br><br><br>
        <p style="color: #065906; font-size: 30px; text-align: center; color: hsl(120, 90%, 16%); font-size: 40px;">
            Discover Saudi Arabia in various ways

        </p>

    </div>
    <br><br><br>

    <div style="display: flex; justify-content: center; align-items: center; height: 50vh; ">

        <div
            style="border: 5px   solid #065506; border-radius: 1000% 1000% 0% 0%;  margin: 0 20px; text-align: center; height: 400px;">
            <a href="{{ route('nature') }}"> <img src="{{ asset('public/ing/v3.jpg') }}" alt="Image 2"
                    style="width: 301px; height: 300px; border-radius: 1000% 1000% 0% 0%;"></a>
            <div style="margin-top: 10px;"><br>
                <h2 style=" font-family: 'Times New Roman'; color: #065906;">Nature and adventure</h2>
            </div>
        </div>

        <div
            style="border: 5px   solid #065506; border-radius: 1000% 1000% 0% 0%;  margin: 0 20px; text-align: center; height: 400px;">
            <a href="{{ route('sea') }}"> <img src="{{ asset('public/ing/v2.jpg') }}" alt="Image 2"
                    style="width: 300px; height: 300px; border-radius: 1000% 1000% 0% 0%;"></a>
            <div style="margin-top: 10px;"><br>
                <h2 style=" font-family: 'Times New Roman'; color: #065906;">Sea and beach</h2>
            </div>
        </div>

        </head>

        <body>




            <div
                style="border: 5px   solid #065506; border-radius: 1000% 1000% 0% 0%;  margin: 0 20px; text-align: center; height: 400px;">
                <a href="{{ route('culture') }}"> <img src="{{ asset('public/ing/v3.jpg') }}" alt="Image 2"
                        style="width: 300px; height: 300px; border-radius: 1000% 1000% 0% 0%;"></a>
                <div style="margin-top: 10px;"><br>
                    <h2 style=" font-family: 'Times New Roman'; color: #065906;">Culture and heritage</h2>


                </div>
            </div>
    </div>
    <div style="display: flex; justify-content: center; align-items: center; height: 60vh;">
        <div style="text-align: center; margin-right: 20px;">
            <img src="{{ asset('public/ing/lo2.jpg') }}" alt="Your Image"
                style="width: 150px; height: 150px; border-radius: 50%;">
        </div>
        <div>
            <h2 style="color: #065906; font-size: 30px; font-family: 'Times New Roman'">What is TripleT?</h2>
            <h4 style="color:#065906 ; font-size: 20px;font-family: 'Times New Roman'">Volunteer tourism site to find
                additional experiences and adventures</h4>
        </div>
    </div>

    </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
</body>
<footer>
    <div class="footer-icons">
        <a href="#"><i class="fa-brands fa-twitter"></i></a>
        <a href="#"><i class="fa-brands fa-tiktok"></i></a>
        <a href="#"><i class="fa-brands fa-instagram"></i></a>
    </div>
</footer>

</html>
