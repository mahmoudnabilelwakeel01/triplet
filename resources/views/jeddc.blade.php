<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{ asset('public/css/bisc.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script> <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>

    <!-- Include FontAwesome library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-t/aFtU6W89BfvHXqmP6F5Zx3GyB7x6fq7k3eb/zgugV5Wxz0Y6BUVig7uGxGQm/43zqeUqjL0d4H3ZL4X08xCQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        .caption h2 {
            color: white;
            text-align: center;
        }

        h2 {
            color: white;
            margin-right: 220px;
            /* هامش من اليمين بقيمة 50 بيكسل */
            margin-left: 220px;
            /* هامش من اليسار بقيمة 50 بيكسل */
        }

        .caption h2 {
            color: white;
            text-align: center;
            font-family: Arial, sans-serif;
            /* تغيير نوع الخط إلى Arial أو أي نوع آخر */
            font-size: 17px;
            /* تصغير حجم النص إلى 18 بيكسل أو أي قيمة تراها مناسبة */
            line-height: 1.5;
            word-spacing: 2px;
            /* تباعد بين الحروف بقيمة 1 بيكسل، يمكنك تغيير القيمة حسب الحاجة */
        }

        body,
        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        li,
        ul,
        ol,
        div,
        span {
            font-family: "Times New Roman", Times, serif;
        }
    </style>

</head><!--/head-->

<body>

    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br>
    <br>





    <!--.preloader-->
    <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
    <!--/.preloader-->

    <header id="home">
        <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active" style="background-image: url({{ asset('public/img/jj.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">Jeddah Museum and our sweet days The Museum of Jeddah and Our
                            Sweet Days is one of the houses that was turned into museums with all the collectibles it
                            kept, as if it were a case of fixing time in the country area. Although it shares the same
                            place and historical era with the Nassif House, it is unique in having more social and
                            coexisting collectibles. You can show your children on your family tour to The gramophone,
                            canned food, old coins, and the first versions of popular soft drinks.</h2>

                    </div>
                </div>
                <div class="item" style="background-image: url({{ asset('public/img/jjj.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">Al Tayebat City Museum The Al-Tayebat City Museum, which
                            extends over a vast area in the Al-Faisaliah neighborhood, represents an imitation of
                            ancient Jeddah architecture, a small city within a city, as it includes 150 suites and 365
                            rooms, and combines heritage and civilization, and its internal courtyards embody
                            interesting time periods of Saudi heritage and precious Islamic art. It took a while to
                            collect The amazing works of art and antiques and the wonderful historical relics for about
                            15 years... the most beautiful souvenirs you will find as you wander the corridors of this
                            museum. In the same area, two minutes away inside Taybat City, is the Sheikh Abdul Raouf
                            Khalil Museum. He is the one who came up with the idea of ​​the Taybat City Museum, as he
                            began the journey of collecting and documenting antiquities about 44 years ago, and it
                            houses exclusive manuscripts and antiques that are unique to this museum in the entire
                            Middle East.</h2>

                    </div>
                </div>
                <div class="item" style="background-image: url({{ asset('public/img/jjjj.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">Blue dolphins at the South Obhur Corniche On your way to
                            Obhur South, you will see the most wonderful artistic masterpieces, such as: the Camel
                            Roundabout, and nearby is a distinguished group of restaurants and cafes in Marina Avenue
                            Mall, such as the Dijon Café with a French atmosphere, and the Kunafa Factory, which is
                            famous for serving the most delicious types of sweets. You will also see when you reach the
                            Corniche sessions that are distinguished by their simplicity. Its shape resembles boats, and
                            if you are lucky, you may catch a glimpse of blue dolphins swimming in the sea in an
                            enchanting scene. If you want to have a marine adventure or even eat one of your meals on
                            the deck of a ship sailing in the Red Sea, then Marsa Al-Ahlam is your ideal choice.</h2>


                    </div>
                </div>
            </div>

            <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

        </div>
    </header><!--/#home-->

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
    <script type="text/javascript" src="{{ asset('public/js/bisc.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/bootstrap.bisc.js') }}"></script>

</body>

</html>
