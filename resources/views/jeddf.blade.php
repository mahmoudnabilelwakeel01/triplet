<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>THE BEST RESTAURANTS AND CAFES</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Free HTML Templates" name="keywords">
    <meta content="Free HTML Templates" name="description">


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>


    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <!-- Google Web Fonts -->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>

    <!-- Include FontAwesome library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-t/aFtU6W89BfvHXqmP6F5Zx3GyB7x6fq7k3eb/zgugV5Wxz0Y6BUVig7uGxGQm/43zqeUqjL0d4H3ZL4X08xCQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link
        href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@400;700&family=Roboto:wght@400;700&display=swap"
        rel="stylesheet">

    <!-- Icon Font Stylesheet -->

    <!-- Libraries Stylesheet -->

    <!-- Customized Bootstrap Stylesheet -->

    <!-- Template Stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}">

    <link href="{{ asset('public/css/st.css') }}" rel="stylesheet">

</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>



    <!-- Testimonial Start -->
    <!-- Testimonial Start -->
    <br>
    <br>

    <div class="container-fluid py-5">
        <div class="container">
            <div class="text-center mx-auto mb-5" style="max-width: 500px;">
                <h5 class="d-inline-block text-primary text-uppercase border-bottom border-5"
                    style="color: black !important;">THE BEST RESTAURANTS AND CAFES</h5>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="owl-carousel testimonial-carousel">
                        <div class="testimonial-item text-center">
                            <img class="img-fluid mx-auto mb-5 rounded-0" src="{{ asset('public/img/img2.png') }}" alt="">
                            <p class="fs-4 fw-normal" style="color: black;">Broots Cafe in the Al Nahda neighborhood is
                                unique in its drinks and products made from chocolate, and their fresh baked goods have
                                high ratings, such as: vegan banana cakes, and gluten-free chocolate cake. You can enjoy
                                the African decor and enjoy mixing flavors in their coffee, such as: lavender coffee,
                                hazelnut latte, The coffee is strong and you will be deliciously spoiled for choice from
                                their list of signature items.</p>
                            <hr class="w-25 mx-auto" style="background-color: black;">
                            <h3 class="text-center" style="color: black;">Bruts (coffee and cocoa)
                            </h3>
                        </div>
                        <div class="testimonial-item text-center">
                            <img class="img-fluid mx-auto mb-5 rounded-0" src="{{ asset('public/img/img3.png') }}" alt="">
                            <p class="fs-4 fw-normal" style="color: black;">For a more intimate and simple atmosphere,
                                you should visit the Cup and Sofa Café. It has fresh baked goods, good coffee, and
                                several types of sweets that deserve to be on your menu. It's in the middle of the city,
                                has options and moods, and its staff are very nice.</p>
                            <hr class="w-25 mx-auto" style="background-color: black;">
                            <h3 class="text-center" style="color: black;">A cup and a sofa</h3>
                        </div>
                        <div class="testimonial-item text-center">
                            <p class="fs-4 fw-normal" style="color: black;">Sakura Restaurant is located on the ground
                                floor of the Crowne Plaza Hotel, Al Hamra Corniche Road. It represents a luxurious
                                destination for fans of the authentic Japanese restaurant, allowing its guests to enjoy
                                the dishes on the menu, such as: lobster rolls, "sushi" and caviar.</p>
                            <hr class="w-25 mx-auto" style="background-color: black;">
                            <h3 class="text-center" style="color: black;">Sakura Restaurant</h3>
                        </div>
                        <div class="testimonial-item text-center">
                            <p class="fs-4 fw-normal" style="color: black;">Mukami Lounge Restaurant is located on
                                King Abdulaziz Road, and is distinguished by its elegant entrance made of warm-colored
                                wood. The restaurant also serves authentic Japanese cuisine, which includes the most
                                delicious types of "sushi","sashimi", and "ramen".</p>
                            <hr class="w-25 mx-auto" style="background-color: black;">
                            <h3 class="text-center" style="color: black;">Restaurant and lounge</h3>
                        </div>
                        <div class="testimonial-item text-center">
                            <p class="fs-4 fw-normal" style="color: black;">Gold Sushi Club features an entrance
                                inspired by traditional Tokyo storefronts, decorated with lanterns and handwritten
                                signs, serving a variety of delicious Japanese dishes.</p>
                            <hr class="w-25 mx-auto" style="background-color: black;">
                            <h3 class="text-center" style="color: black;">Gold Sushi Club Restaurant</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Testimonial End -->
    <!-- Testimonial End -->


    <!-- Footer Start -->



    <!-- Back to Top -->
    <a href="#" class="btn btn-lg btn-primary btn-lg-square back-to-top"><i class="bi bi-arrow-up"></i></a>

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

    <!-- Template Javascript -->
    <script src="{{ asset('public/js/caf.js') }}"></script>

</body>

</html>
