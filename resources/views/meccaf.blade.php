<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="{{ asset('public/css/bisc.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script> <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
  <![endif]-->
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="css/your.css">

    <!-- Include FontAwesome library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"
        integrity="sha512-t/aFtU6W89BfvHXqmP6F5Zx3GyB7x6fq7k3eb/zgugV5Wxz0Y6BUVig7uGxGQm/43zqeUqjL0d4H3ZL4X08xCQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        /* أضف هذا الكود لتحديد لون زر "Find out more" */
        .message {
            width: 1240px;
            height: 90px;
            padding-left: 400px;
            color: black;
            margin-top: 100px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 30px;
            line-height: 70px;
        }

        .line {
            border-bottom: 1px solid black;
            /* سمك ولون الخط */
            width: 50%;
            /* عرض الخط (يمكن تعديله حسب الحاجة) */
            margin-top: 2px;
            position: fixed;
            margin-left: 400px;
        }

        .caption h2 {
            color: white;
            text-align: center;
        }

        h2 {
            color: white;
            margin-right: 220px;
            /* هامش من اليمين بقيمة 50 بيكسل */
            margin-left: 220px;
            /* هامش من اليسار بقيمة 50 بيكسل */
        }

        .caption h2 {
            color: white;
            text-align: center;
            font-family: Arial, sans-serif;
            /* تغيير نوع الخط إلى Arial أو أي نوع آخر */
            font-size: 17px;
            /* تصغير حجم النص إلى 18 بيكسل أو أي قيمة تراها مناسبة */
            line-height: 1.5;
            word-spacing: 2px;
            /* تباعد بين الحروف بقيمة 1 بيكسل، يمكنك تغيير القيمة حسب الحاجة */
        }
    </style>

</head><!--/head-->

<body>
    <nav>
        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>
            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br>
    <br>


    <!--.preloader-->
    <div class="preloader"> <i class="fa fa-circle-o-notch fa-spin"></i></div>
    <!--/.preloader-->

    <header id="home">
        <div id="home-slider" class="carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner">
                <div class="item active" style="background-image: url({{ asset('public/img/picm.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">Tourism in MeccaMecca Al-Mukarramah is the mother of
                            villages, and the most sacred city in the world for Muslims. That city embraces the Sacred
                            Mosque with its Holy Kaaba, the purest place on earth ever. Therefore, tourism in Mecca
                            occupies a special place in the hearts of millions, as it is an activity that combines the
                            spirituality of religion, the nobility of the past, and the modernity of the future. All of
                            this contributed to the inclusion of Mecca among the list of the most attractive cities in
                            the world for travelers.</h2>

                    </div>
                </div>
                <div class="item" style="background-image: url({{ asset('public/img/mm.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">The mall is one of the most famous tourist places in all of
                            Saudi Arabia, not just in Mecca, as it is visited by millions of shopping enthusiasts when
                            traveling to Mecca, because it includes the most luxurious international and local brands
                            and the most famous shops, restaurants, and entertainment venues. Mecca Mall is located on
                            King Abdullah Road, only 12 km away from the Grand Mosque, and it operates daily from nine
                            in the morning until ten in the evening to please the public. In addition to the
                            multiplicity of shopping and entertainment facilities, the mall offers other services such
                            as a first aid unit, facilities for the elderly and people with disabilities. Special needs,
                            restrooms for women and children, ATM services and currency exchange, all in addition to the
                            largest indoor games park in the Middle East, which has something suitable for all ages.
                        </h2>

                    </div>
                </div>
                <div class="item" style="background-image: url({{ asset('public/img/mmm.png') }})">
                    <div class="caption">
                        <h2 class="animated fadeInLeftBig">It is also called “Al Hokair Fun Town Makkah” Park. It is
                            one of the most important tourist destinations in Makkah. It is an entertainment city and a
                            games city located in the heart of the park of the Arabian Peninsula, located at the
                            crossroads between Makkah-Madina Road and Makkah-Taif Road, and it was established by Al
                            Hokair Company. For tourism projects in the year 2001 AD. Al Hokair amusement park includes
                            closed halls equipped with the latest entertainment and technology, in addition to a huge
                            collection of the most famous electric amusement rides, in addition to rest areas,
                            restaurant and café areas, and family gathering areas, all of which adds to traveling to
                            Mecca family entertainment fun in conjunction with its spiritual beauty.</h2>


                    </div>
                </div>
            </div>

            <a class="left-control" href="#home-slider" data-slide="prev"><i class="fa fa-angle-left"></i></a>
            <a class="right-control" href="#home-slider" data-slide="next"><i class="fa fa-angle-right"></i></a>

        </div>
    </header><!--/#home-->
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>

    <script type="text/javascript" src="{{ asset('public/js/bisc.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/bootstrap.bisc.js') }}"></script>

</body>

</html>
