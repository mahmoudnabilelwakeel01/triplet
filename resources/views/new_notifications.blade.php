<!DOCTYPE html>

<head>
    <meta charset="UTF-8">
    <title>TripleT</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/New-notifications.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">

</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @if (auth()->guard('web')->user()->type == 3 || auth()->guard('web')->user()->type == 4)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @else
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif

                @endif

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br>
    <br>
    <br>
    <br>

    <div class="search">
        <label>Request number</label>
        <select id="requestNumberSelect">
            <option value="">Choose request number</option>
            @foreach ($notifications as $notification)
                <option value="{{ $notification->offer_id }}">{{ $notification->offer_id ?? '' }}</option>
            @endforeach
        </select>
    </div>
    <div style="display: grid; place-items: center;">
        @foreach ($notifications as $notification)
            <div class="row number-{{ $notification->offer_id ?? '' }}" style="width: 105ch">
                <div >
                    <div class="new-message-box">
                        @if ($notification->status === 1)
                            <div class="new-message-box-success">
                                <div class="info-tab tip-icon-success" title="success"><i></i></div>
                                <div class="tip-box-success">
                                    <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                                    <p>request {{ $notification->offer_id ?? '' }} has been accepted
                                        @if (auth()->guard('web')->user()->type == 2)
                                            <a class="btn btn-sm" href="{{ route('card') }}"> " Pay now "</a>
                                        @endif
                                    </p>
                                </div>
                            </div>
                        @elseif($notification->status === 0)
                            <div class="new-message-box-danger">
                                <div class="info-tab tip-icon-danger" title="error"><i></i></div>
                                <div class="tip-box-danger">
                                    <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                                    <p>request {{ $notification->offer_id ?? '' }} has been rejected
                                        <a class="btn btn-sm" href="{{ route('card') }}"> </a>
                                    </p>
                                </div>
                            </div>
                        @else
                            <div class="new-message-box-success">
                                <div class="info-tab tip-icon-success" title="success"><i></i></div>
                                <div class="tip-box-success">
                                    <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                                    <p>request {{ $notification->offer_id ?? '' }} has been sent
                                    </p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <!-- -->
    {{-- <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="new-message-box">
                <div class="new-message-box-success">
                    <div class="info-tab tip-icon-success" title="success"><i></i></div>
                    <div class="tip-box-success">
                        <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                        <p>5678 Your request has been accepted
                            <a class="btn btn-sm" href="{{ route('card') }}"> " Pay now "</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="new-message-box">
                <div class="new-message-box-success">
                    <div class="info-tab tip-icon-success" title="success"><i></i></div>
                    <div class="tip-box-success">
                        <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                        <p>8965 Your request has been accepted
                            <a class="btn btn-sm" href="{{ route('card') }}"> " Pay now "</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="new-message-box">
                <div class="new-message-box-danger">
                    <div class="info-tab tip-icon-danger" title="error"><i></i></div>
                    <div class="tip-box-danger">
                        <!--<p><strong>Tip:</strong> If you want to enable the fading transition effect while closing the alert boxes, apply the classes <code>.fade</code> and <code>.in</code> to them along with the contextual class.</p>-->
                        <p>9075 Your request has been rejected
                            <a class="btn btn-sm" href="555"> </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    <!-- -->

    <!-- -->

    </div>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
    </div>
    <script>
        document.getElementById('requestNumberSelect').addEventListener('change', function() {
            var selectedNumber = this.value;
            var rows = document.querySelectorAll('.row');

            // If no option is selected, show all rows
            if (!selectedNumber) {
                rows.forEach(function(row) {
                    row.style.display = 'block';
                });
                return;
            }

            // Hide all rows
            rows.forEach(function(row) {
                row.style.display = 'none';
            });

            // Show the row with the selected number
            var selectedRow = document.querySelector('.number-' + selectedNumber);
            if (selectedRow) {
                selectedRow.style.display = 'block';
            }
        });
    </script>

</body>

</html>
