<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">
    <title>TripleT</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/GreatOffer.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/your.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}"> --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<style>
    .card {
        border-radius: 15px;
        /* تطبيق حواف شبه دائرية بقيمة 15 بيكسل */
        overflow: hidden;
        /* للتأكد من أن الصورة لا تتجاوز الحواف الشبه دائرية */
    }

    .dropdown ion-icon {
        font-size: 30px;
        margin-top: 15px;
    }

    .main {
        width: 100%;
        background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
        background-position: center;
        background-size: cover;

    }


    custom-btn {
        background-color: #135313;
        /* لون أخضر داكن */
        color: #ffffff;
        /* لون أبيض للنص */
    }

    .custom-btn:active {
        background-color: #ffffff;
        /* لون الخلفية الأبيض */
        color: #135313;
        /* لون النص الأخضر */
    }

    .slick-custom {
        height: 100vh;
        /* أو أي ارتفاع ترغب فيه */
        margin: 0;
        /* إزالة الهوامش */
        padding: 0;
        /* إزالة الحواف الداخلية */

    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        padding: 2px;
        border-radius: 20px;
        /* زيادة قيمة border-radius لجعل الحواف أكثر دائرية */
        box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
        z-index: 1;
    }

    .dropdown-content a {
        font-size: 14px;
        /* حجم النص */
        color: black;
        padding: 6px 16px;
        /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
        text-decoration: none;
        display: block;
    }

    .dropdown-content a:hover {
        background-color: #f1f1f1;
    }

    .circle-outline {
        font-size: 25px;
    }

    .person-circle-outline {
        font-size: 25px;
    }

    .dropdown:hover .dropdown-content {
        display: block;
    }
</style>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="#">Services</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li style="line-height: 4; position: relative;">
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .card {
            width: 320px;
            height: 420px;
            border-radius: 20px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transform: translateY(0);
            transition: transform 0.3s ease;
            margin: 30px;
        }

        .card:hover {
            transform: translateY(-10px);
        }

        .card img {
            width: 300px;
            height: 250px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-content {
            padding: 20px;
            height: 500px;
            background-color: #fff;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .card-content h2 {
            margin-top: 0;
            margin-left: 70px;
            color: #135313;
        }

        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #135313;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-left: 85px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
        }

        .ss {
            display: flex;
            border-radius: 15px;
            justify-content: center;
        }

        .aa {
            display: flex;
            justify-content: center;
        }
    </style><br><br><br><br><br><br><br><br>
    <p style="color: #065906; font-size: 30px; text-align: center; color: hsl(120, 90%, 16%); font-size: 40px;"> Current
        offers
    </p>
    <br>
    </head>


    <a href="{{ route('offer_tourist_add') }}"> <i class="fa-solid fa-plus"></i></a>
    </head>

    <body>
        <div class="line"></div>
        <br>
        <table>
            <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>City</th>
                    <th>Hour</th>
                    <th>Price</th>
                    <th>currency</th>
                    <th>Photo</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <!-- يمكنك إضافة صفوف إضافية حسب الحاجة -->
                @foreach ($offerTourists as $offerTourist)
                    <tr>
                        <td>
                            @switch($offerTourist->type)
                                @case(1)
                                    Entertainment
                                @break

                                @case(2)
                                    Cultural
                                @break

                                @case(3)
                                    Religious
                                @break

                                @case(4)
                                    Sea
                                @break

                                @case(5)
                                    Environmental
                                @break

                                @case(6)
                                    Mountainous
                                @break

                                @default
                            @endswitch
                        </td>
                        <td>{{$offerTourist->name ?? ''}}</td>
                        <td>{{$offerTourist->city ?? ''}}</td>
                        <td>{{$offerTourist->time ?? ''}}</td>
                        <td>{{$offerTourist->price ?? ''}}</td>
                        <td>{{$offerTourist->currency ?? ''}}</td>
                        <td>
                            <img class="logo-abbr" style="width:60px "
                            @if ($offerTourist->photo) src="{{ asset('public/' . Storage::url($offerTourist->photo)) }}" @endif>

                        </td>
                        <td>
                            <a href="{{route('offer_tourist_edit',$offerTourist->id)}}"><i class="fa-solid fa-pencil"></i></a>
                            <a href="{{route('offer_tourist_delete',$offerTourist->id)}}"><i class="fa-solid fa-trash" style="color: #263e0f;"></i>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        </div>
    </body>

    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>

</html>
