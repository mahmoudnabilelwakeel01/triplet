<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/offer.css') }}">
    <title>Offer</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <style>
        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 2px 0;

            bottom: 0;
            left: 0;

        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        .card {
            border-radius: 15px;
            /* تطبيق حواف شبه دائرية بقيمة 15 بيكسل */
            overflow: hidden;
            /* للتأكد من أن الصورة لا تتجاوز الحواف الشبه دائرية */
        }

        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        nav {
            background-color: #ffffff;
            /* تغيير لون خلفية القائمة إلى الأبيض */
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 79px;
            padding: 0.5px;
            box-sizing: border-box;
            border-bottom: 1px solid #e0ede2;
            max-width: 12000px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            z-index: 1000;
        }

        nav ul {
            list-style: none;
            margin-right: 80px;
            position: relative;
            /* إضافة */
        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;
        }

        .logo {
            position: absolute;
            top: 5px;
            left: 0;
        }

        .logo img {
            width: 65px;
            height: 65px;

        }

        custom-btn {
            background-color: #135313;
            /* لون أخضر داكن */
            color: #ffffff;
            /* لون أبيض للنص */
        }

        .custom-btn:active {
            background-color: #ffffff;
            /* لون الخلفية الأبيض */
            color: #135313;
            /* لون النص الأخضر */
        }

        .slick-custom {
            height: 100vh;
            /* أو أي ارتفاع ترغب فيه */
            margin: 0;
            /* إزالة الهوامش */
            padding: 0;
            /* إزالة الحواف الداخلية */

        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            padding: 2px;
            border-radius: 20px;
            /* زيادة قيمة border-radius لجعل الحواف أكثر دائرية */
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            font-size: 14px;
            /* حجم النص */
            color: black;
            padding: 6px 16px;
            /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .circle-outline {
            font-size: 25px;
        }

        .person-circle-outline {
            font-size: 25px;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="#">Services</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li style="line-height: 4; position: relative;">
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .card {
            width: 320px;
            height: 420px;
            border-radius: 20px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transform: translateY(0);
            transition: transform 0.3s ease;
            margin: 30px;
        }

        .card:hover {
            transform: translateY(-10px);
        }

        .card img {
            width: 300px;
            height: 250px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-content {
            padding: 20px;
            height: 500px;
            background-color: #fff;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .card-content h2 {
            margin-top: 0;
            margin-left: 70px;
            color: #135313;
        }

        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #135313;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-left: 85px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
        }

        .ss {
            display: flex;
            border-radius: 15px;
            justify-content: center;
        }

        .aa {
            display: flex;
            justify-content: center;
        }
    </style>
    <div class="header"></div>
    <div class="message">Your Offer</div>
    <div class="products-container">
        <div class="product" data-name="p-1">
            <form action="{{route('offer_tourist_store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <table>
                    <tr>
                        <th>*Type of Trip</th>
                        <td>
                            <select name="type" required>
                                <option value="">Type of Trip</option>
                                <option value="1"> Entertainment trip</option>
                                <option value="2"> Cultural trip</option>
                                <option value="3">Religious trip</option>
                                <option value="4">Sea trip</option>
                                <option value="5">Environmental trip</option>
                                <option value="6">Mountainous trip</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>*Name Trip</th>
                        <td><input type="text" name="name" required></td>
                    </tr>
                    <tr>
                        <th>*City</th>
                        <td>
                            <select id="city" name="city" required>
                                <option value="">City</option>
                                <option value="Tabuk"> Tabuk</option>
                                <option value="Abha">Abha</option>
                                <option value="Taif">Taif</option>
                                <option value="Mecca">Mecca</option>
                                <option value="Jeddah">Jeddah</option>
                                <option value="Riyadh">Riyadh</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th>*Hours</th>
                        <td>
                            <input type="time" id="timePicker" name="time" lang="en" required>
                        </td>
                    </tr>
                    <tr>
                        <th>*photo</th>
                        <td>
                            <label for="order_image" class="btn btn-success">Choose File</label>
                            <input type="file" id="order_image" name="photo" style="display: none;"
                                onchange="updateFileName(this)" accept="image/*" required>
                            <span id="file-name"></span>
                        </td>
                    </tr>
                    <tr>
                        <th>*price</th>
                        <td>
                            <input type="text" min="0" step="0.01" style="width:110px;"  name="price" required>
                            <select id="currency" name="currency" style="width: 93px; height:36px; margin-right:40px;" required >
                                <option value=""> Currency </option>
                                <option value="SAR"> SAR </option>
                                <option value="USD"> USD </option>
                                <option value="EUR"> EUR </option>
                                <option value="GBP"> GBP </option>
                            </select>
                        </td>
                    </tr>
                </table>
                 <button type="submit">Send</button>
            </form>
        </div>
    </div>

    <script src="{{ asset('public/js/of.js') }}"></script>
    <br><br>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
    </div>
</body>

</html>

</body>

</html>
