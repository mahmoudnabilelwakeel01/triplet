<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/offer.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">
    <title>Offer</title>
    <style>
        .card {
            border-radius: 15px;
            /* تطبيق حواف شبه دائرية بقيمة 15 بيكسل */
            overflow: hidden;
            /* للتأكد من أن الصورة لا تتجاوز الحواف الشبه دائرية */
        }

        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        custom-btn {
            background-color: #135313;
            /* لون أخضر داكن */
            color: #ffffff;
            /* لون أبيض للنص */
        }

        .custom-btn:active {
            background-color: #ffffff;
            /* لون الخلفية الأبيض */
            color: #135313;
            /* لون النص الأخضر */
        }

        .slick-custom {
            height: 100vh;
            /* أو أي ارتفاع ترغب فيه */
            margin: 0;
            /* إزالة الهوامش */
            padding: 0;
            /* إزالة الحواف الداخلية */

        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            padding: 2px;
            border-radius: 20px;
            /* زيادة قيمة border-radius لجعل الحواف أكثر دائرية */
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            font-size: 14px;
            /* حجم النص */
            color: black;
            padding: 6px 16px;
            /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .circle-outline {
            font-size: 25px;
        }

        .person-circle-outline {
            font-size: 25px;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="#">Services</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li style="line-height: 4; position: relative;">
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .card {
            width: 320px;
            height: 420px;
            border-radius: 20px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transform: translateY(0);
            transition: transform 0.3s ease;
            margin: 30px;
        }

        .card:hover {
            transform: translateY(-10px);
        }

        .card img {
            width: 300px;
            height: 250px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-content {
            padding: 20px;
            height: 500px;
            background-color: #fff;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .card-content h2 {
            margin-top: 0;
            margin-left: 70px;
            color: #135313;
        }

        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #135313;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-left: 85px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
        }

        .ss {
            display: flex;
            border-radius: 15px;
            justify-content: center;
        }

        .aa {
            display: flex;
            justify-content: center;
        }
    </style>

    <div class="header"></div>
    <div class="message">Modify Offer</div>
    <form action="{{ route('offer_volunteer_update',[ $type,$offerVolunteer->id]) }}" method="post" enctype="multipart/form-data">
        @csrf

        <table>
            <tr>
                <th>*Type of Trip</th>
                <td>
                    <select name="type" onchange="showLanguages(this.value)" required>
                        <option value="">Type of Trip</option>
                        <option value="1"@if ($offerVolunteer->type == 1) selected @endif>Education</option>
                        <option value="2"@if ($offerVolunteer->type == 2) selected @endif>Healthy</option>
                        <option value="3"@if ($offerVolunteer->type == 3) selected @endif>Other</option>
                    </select>
                </td>
            </tr>
            <tr id="languagesRow" @if ($offerVolunteer->type != 1) style="display:none;" @endif>
                <th>*Languages</th>
                <td>
                    <select name="language">
                        <option value="1"@if ($offerVolunteer->language == 1) selected @endif>English</option>
                        <option value="2"@if ($offerVolunteer->language == 2) selected @endif>French</option>
                        <option value="3"@if ($offerVolunteer->language == 3) selected @endif>Chinese</option>
                        <option value="4"@if ($offerVolunteer->language == 4) selected @endif>Turkish</option>
                        <option value="5"@if ($offerVolunteer->language == 5) selected @endif>Russian</option>
                        <option value="6"@if ($offerVolunteer->language == 6) selected @endif>Indian</option>
                    </select>
                </td>
            </tr>
            <tr>
                <th>*Name Trip</th>
                <td><input type="text" name="name" value="{{ $offerVolunteer->name ?? '' }}" required></td>
            </tr>
            <tr>
                <th>*City</th>
                <td>
                    <select id="city" name="city" required>
                        <option value="">City</option>
                        <option value="Tabuk" @if($offerVolunteer->city == "Tabuk") selected @endif> Tabuk</option>
                        <option value="Abha" @if($offerVolunteer->city == "Abha") selected @endif>Abha</option>
                        <option value="Taif" @if($offerVolunteer->city == "Taif") selected @endif>Taif</option>
                        <option value="Mecca" @if($offerVolunteer->city == "Mecca") selected @endif>Mecca</option>
                        <option value="Jeddah" @if($offerVolunteer->city == "Jeddah") selected @endif>Jeddah</option>
                        <option value="Riyadh" @if($offerVolunteer->city == "Riyadh") selected @endif>Riyadh</option>
                    </select>

                </td>
            </tr>
            <tr>
                <th>*Duration</th>
                <td>
                    <input type="text" id="timePicker" lang="en" name="time"
                        value="{{ $offerVolunteer->time ?? '' }}" required>
                </td>
            </tr>
            <tr>
                <th>*Photo</th>
                <td>
                    <label for="order_image" class="btn btn-success">Choose File</label>
                    <input type="file" id="order_image" name="photo" style="display: none;"
                        onchange="updateFileName(this)" accept="image/*">
                    <span id="file-name"></span>
                </td>
            </tr>
            <tr>
                <th>*Reward</th>
                <td><input type="text" name="reward" value="{{ $offerVolunteer->reward ?? '' }}"></td>
            </tr>

        </table>

        <script>
            function showLanguages(value) {
                var languagesRow = document.getElementById("languagesRow");
                if (value === "1") {
                    languagesRow.style.display = "table-row";
                } else {
                    languagesRow.style.display = "none";
                }
            }
        </script>
        <button type="submit">Send</button>
    </form>

    {{-- <script src="of.js"></script> --}}
    <br><br>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
    </div>
</body>

</html>
</body>

</html>
