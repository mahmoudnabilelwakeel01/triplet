<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="UTF-8">
    <title>Profile</title>
    <link rel="stylesheet" href="{{ asset('public/css/profile.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/Home.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-alpha1/dist/js/bootstrap.bundle.min.js">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>

</head>

<body>

    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>
                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{ route('profile') }}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset

                            <a href="{{ route('admin.logout') }}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
    <br><br><br><br>

    </nav>
    @if (Session::has('success'))
        <div class="alert alert-success">
            {{ Session::get('success') }}
        </div>
    @endif

    @if (Session::has('error'))
        <div class="alert alert-danger">
            {{ Session::get('error') }}
        </div>
    @endif
    <div class="container rounded bg-white mt-5 mb-5">
        <div class="row">
            <div class="col-md-3 border-right">
                <div class="d-flex flex-column align-items-center text-center p-3 py-5"><img class="rounded-circle mt-5"
                        width="150px"
                        src="https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-glasses-profile.jpg"><span
                        class="font-weight-bold">{{ $user->name ?? '' }}</span><span
                        class="text-black-50">{{ $user->email ?? '' }}</span><span> </span></div>
            </div>
            <div class="col-md-5 border-right">
                <form action="{{ route('profileSave') }}" method="post">
                    @csrf
                    <div class="p-3 py-5">
                        <div class="d-flex justify-content-between align-items-center mb-3">
                            <h4 class="text-right">Profile</h4>
                        </div>
                        <div class="row mt-2">
                            <div class="col-md-6">
                                <label class="labels">Name</label>
                                <input type="text" class="form-control" placeholder="first name"
                                    value="{{ $user->name ?? '' }} " name="name">
                            </div>
                            <div class="col-md-6">
                                <label class="labels">Surname</label>
                                <input type="text" class="form-control" value="{{ $user->surname ?? '' }}"
                                    placeholder="surname" name="surname">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-12">
                                <label class="labels">Mobile Number</label>
                                <input type="text" class="form-control" placeholder="enter phone number"
                                    value="{{ $user->mobile ?? '' }}" name="mobile">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Address Line 1</label>
                                <input type="text" class="form-control" placeholder="enter address line 1"
                                    value="{{ $user->address1 ?? '' }}" name="address1">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Address Line 2</label>
                                <input type="text" class="form-control" placeholder="enter address line 2"
                                    value="{{ $user->address2 ?? '' }}" name="address2">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Postcode</label>
                                <input type="text" class="form-control" placeholder="enter address line 2"
                                    value="{{ $user->postcode ?? '' }}" name="postcode">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">State</label>
                                <input type="text" class="form-control" placeholder="enter address line 2"
                                    value="{{ $user->state ?? '' }}" name="state">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Area</label>
                                <input type="text" class="form-control" placeholder="enter address line 2"
                                    value="{{ $user->area ?? '' }}" name="area">
                            </div>
                            <div class="col-md-12">
                                <label class="labels">Email </label>
                                <input type="text" class="form-control" placeholder="enter email id"
                                    value="{{ $user->email ?? '' }}" name="email">
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <label class="labels">Country</label>
                                <input type="text" class="form-control" placeholder="country"
                                    value="{{ $user->country ?? '' }}" name="country">
                            </div>
                            <div class="col-md-6">
                                <label class="labels">State/Region</label>
                                <input type="text" class="form-control" placeholder="State/Region"
                                    value="{{ $user->stateRegion ?? '' }}" name="stateRegion">
                            </div>
                        </div>
                        <div class="mt-5 text-center">
                            <button class="btn btn-primary profile-button" type="submit"
                                style="background-color: #135313; border: #135313;"> Save Profile
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
</body>

</html>
