<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>TripleT</title>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>






    {{-- <style>
        table {
            width: 80%;
            border-collapse: collapse;
            margin: 50px auto;
            background-color: #f9f9f9;
        }

        th,
        td {
            padding: 12px 15px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr:hover {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .fa-trash,
        .fa-envelope,
        .fa-pencil,
        .fa-star {
            font-size: 20px;
            margin-right: 10px;
            color: #135313;
        }

        .fa-plus {
            font-size: 30px;
            margin: 0 10px;
            color: rgb(7, 80, 7);
            margin-left: 400px;
            margin-top: 5px;



        }
    </style> --}}

    <style>
        table {
            border-collapse: black;
            width: 50%;
            /* عرض الجدول (يمكن تعديله حسب الحاجة) */
            margin: 20px;
            /* المسافة من الأطراف */
            position: absolute;

            margin-left: 400px;

        }

        th,
        td {
            padding: 15px;
            text-align: center;
        }

        th {
            background-color: rgb(7, 80, 7);
            /* لون الخلفية للعناصر العنوانية (Header) */
            color: white;
            border-color: beige;
            /* لون النص للعناصر العنوانية */
        }

        tr:nth-child(even) {
            background-color: white;
            border-color: rgb(193, 193, 179);
            /* لون الخلفية للصفوف الزوجية */
        }

        .fa-trash {
            color: rgb(7, 80, 7);
        }

        .fa-pencil {
            color: rgb(7, 80, 7);
        }

        .fa-thin fa-star {
            color: rgb(7, 80, 7);

        }
    </style>
    <table style="margin-top: 5%;padding-bottom:100px">
        <div class="message">
            Previous Requests
        </div>
        <style>
            .dropdown-content {
                display: none;
                position: absolute;
                background-color: #f9f9f9;
                min-width: 160px;
                /* عرض القائمة المنسدلة */
                padding: 2px;
                /* تباعد داخلي - تقليل التباعد العمودي بشكل إضافي */
                border-radius: 5px;
                /* تدوير الزوايا */
                box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
                z-index: 1;
            }

            .dropdown-content a {
                font-size: 14px;
                /* حجم النص */
                color: black;
                padding: 6px 16px;
                /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
                text-decoration: none;
                display: block;
            }

            .dropdown-content a:hover {
                background-color: #f1f1f1;
            }

            .dropdown:hover .dropdown-content {
                display: block;
            }

            .message {
                width: 1240px;
                height: 90px;
                padding-top: 10%;
                padding-left: 400px;
                color: black;
                font-family: 'Times New Roman', Times, serif;
                font-size: 30px;
                line-height: 70px;
            }

            .line {
                border-bottom: 1px solid black;
                /* سمك ولون الخط */
                width: 50%;
                /* عرض الخط (يمكن تعديله حسب الحاجة) */
                margin-top: 5%;
                position: absolute;
                margin-left: 400px;
            }
        </style>
        <div class="line"></div>
        <br>

        <thead>
            <tr>
                <th>id request</th>
                <th>request</th>
                <th>ٍٍٍRequestStatus</th>
                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            <!-- يمكنك إضافة صفوف إضافية حسب الحاجة -->
            @foreach ($offerVolunteerRequests as $offerVolunteerRequest)

                <tr>
                    <td>{{ $offerVolunteerRequest->id ?? '' }}</td>
                    <td>{{ $offerVolunteerRequest->offerVolunteer->name ?? '' }}</td>
                    <td>
                        @if ($offerVolunteerRequest->status === 0)
                            Rejected
                        @elseif($offerVolunteerRequest->status === 1)
                            Accepted
                        @else
                            Waiting
                        @endif

                    </td>

                    <td><a href="{{ route('delete', $offerVolunteerRequest->id) }}"><i
                                class="fa-solid fa-trash"></i></a>
                    </td>
                </tr>
                @endforeach

        </tbody>
    </table>




<br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<footer>
    <div class="footer-icons">
        <a href="#"><i class="fa-brands fa-twitter"></i></a>
        <a href="#"><i class="fa-brands fa-tiktok"></i></a>
        <a href="#"><i class="fa-brands fa-instagram"></i></a>
    </div>
</footer>
</body>

</html>
