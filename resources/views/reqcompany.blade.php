<!DOCTYPE html>
<html>

<head>

    <meta charset="UTF-8">
    <title>TripleT</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/re.css') }}">
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;
            position: fixed;
            bottom: 0;
            left: 0;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        .menu-link.active {
            color: green;
        }

        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>
</head>

<body>

    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset
                <li><a href="#">Services</a></li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li style="line-height: 4; position: relative;">
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .card {
            width: 320px;
            height: 420px;
            border-radius: 20px;
            overflow: hidden;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            transform: translateY(0);
            transition: transform 0.3s ease;
            margin: 30px;
        }

        .card:hover {
            transform: translateY(-10px);
        }

        .card img {
            width: 300px;
            height: 250px;
            border-bottom-left-radius: 0;
            border-bottom-right-radius: 0;
        }

        .card-content {
            padding: 20px;
            height: 500px;
            background-color: #fff;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .card-content h2 {
            margin-top: 0;
            margin-left: 70px;
            color: #135313;
        }

        .btn {
            display: inline-block;
            padding: 10px 20px;
            background-color: #135313;
            color: #fff;
            text-decoration: none;
            border-radius: 5px;
            margin-left: 85px;
            transition: background-color 0.3s ease;
        }

        .btn:hover {
            color: hsl(129, 30%, 53%);
            background: rgba(0, 0, 0, 0, 7);
        }

        .ss {
            display: flex;
            border-radius: 15px;
            justify-content: center;
        }

        .aa {
            display: flex;
            justify-content: center;
        }
    </style>
    <br><br><br><br><br><br><br><br>






    <style>
        table {
            width: 80%;
            border-collapse: collapse;
            margin: 50px auto;
            background-color: #f9f9f9;
        }

        th,
        td {
            padding: 12px 15px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr:hover {
            background-color: #f2f2f2;
        }

        tr:nth-child(even) {
            background-color: #f2f2f2;
        }

        .fa-trash,
        .fa-envelope,
        .fa-pencil,
        .fa-star {
            font-size: 20px;
            margin-right: 10px;
            color: #135313;
        }

        .fa-plus {
            font-size: 30px;
            margin: 0 10px;
            color: rgb(7, 80, 7);
            margin-left: 400px;
            margin-top: 5px;



        }
    </style>


    <div class="message">
        Previous Requests
    </div>
    <style>
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            /* عرض القائمة المنسدلة */
            padding: 2px;
            /* تباعد داخلي - تقليل التباعد العمودي بشكل إضافي */
            border-radius: 5px;
            /* تدوير الزوايا */
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            font-size: 14px;
            /* حجم النص */
            color: black;
            padding: 6px 16px;
            /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }

        .message {
            width: 1240px;
            height: 90px;
            padding-left: 400px;
            color: black;
            margin-top: 100px;
            font-family: 'Times New Roman', Times, serif;
            font-size: 30px;
            line-height: 70px;
        }

        .line {
            border-bottom: 1px solid black;
            /* سمك ولون الخط */
            width: 50%;
            /* عرض الخط (يمكن تعديله حسب الحاجة) */
            margin-top: 2px;
            position: fixed;
            margin-left: 400px;
        }
    </style>


    <div class="line"></div>
    <br>
    <style>
        table {
            border-collapse: black;
            width: 50%;
            /* عرض الجدول (يمكن تعديله حسب الحاجة) */
            margin: 20px;
            /* المسافة من الأطراف */
            position: fixed;
            margin-left: 400px;

        }

        th,
        td {
            padding: 15px;
            text-align: center;
        }

        th {
            background-color: rgb(7, 80, 7);
            /* لون الخلفية للعناصر العنوانية (Header) */
            color: white;
            border-color: beige;
            /* لون النص للعناصر العنوانية */
        }

        tr:nth-child(even) {
            background-color: white;
            border-color: rgb(193, 193, 179);
            /* لون الخلفية للصفوف الزوجية */
        }

        .fa-trash {
            color: rgb(7, 80, 7);
        }

        .fa-pencil {
            color: rgb(7, 80, 7);
        }

        .fa-thin fa-star {
            color: rgb(7, 80, 7);

        }
    </style>

    <table>
        <thead>
            <tr>
                <th>Request id</th>
                <th> Offer</th>
                <th>ٍٍٍdetails</th>
                <th>ٍٍٍRequestStatus</th>

                <th>Action</th>

            </tr>
        </thead>
        <tbody>
            <!-- يمكنك إضافة صفوف إضافية حسب الحاجة -->
            @foreach ($offerVolunteerRequests as $offerVolunteerRequest)
                <tr>
                    <td>{{ $offerVolunteerRequest->id ?? '' }}</td>
                    <td>{{ $offerVolunteerRequest->offerVolunteer->name ?? '' }}</td>
                    <td><a href="{{ route('biscompanydetail', $offerVolunteerRequest->id) }}">here</a></td>
                    <td>
                        @if ($offerVolunteerRequest->status === 0)
                            Rejected
                        @elseif($offerVolunteerRequest->status === 1)
                            Accepted
                        @else
                            Waiting
                        @endif
                    </td>
                    <td><a href="{{route('reqcompanydelete',$offerVolunteerRequest->id)}}"><i class="fa-solid fa-trash"></i></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <footer>
        <div class="footer-icons">
            <a href="#"><i class="fa-brands fa-twitter"></i></a>
            <a href="#"><i class="fa-brands fa-tiktok"></i></a>
            <a href="#"><i class="fa-brands fa-instagram"></i></a>
        </div>
    </footer>
</body>

</html>
