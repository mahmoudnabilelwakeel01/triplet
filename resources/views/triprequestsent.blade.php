<!DOCTYPE html>

<head>
    <meta charset="UTF-8">

    <title> Thank you </title>
    <link rel="stylesheet" href="{{ asset('public/css/message-request-1.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('public/css/Home.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">
    <style>
        footer {
            background-color: rgb(243, 243, 243);
            width: 100%;
            padding: 10px 0;

            bottom: 0;
            left: 0;
            top: 1100;
        }

        .footer-icons {
            text-align: center;
            color: #065906;
        }

        .footer-icons a {
            margin: 0 10px;
            color: #065906;
        }

        .footer-icons i {
            font-size: 40px;
            color: rgb(10, 88, 44);
            /* لون الأيقونات */
            padding: 10px;
            border-radius: 50%;

        }

        .card {
            border-radius: 15px;
            /* تطبيق حواف شبه دائرية بقيمة 15 بيكسل */
            overflow: hidden;
            /* للتأكد من أن الصورة لا تتجاوز الحواف الشبه دائرية */
        }

        * {
            margin: 0;
            padding: 2;
            font-family: 'Times New Roman';
            box-sizing: border-box;
            max-width: 12000px;

        }

        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }

        .main {
            width: 100%;
            background: linear-gradient(to top, rgba(255, 255, 255, 0.5));
            background-position: center;
            background-size: cover;

        }

        body {
            max-width: 100%;
            /* تعديل القيمة إلى 100% بدلاً من 1000px */
            margin: 0;
            padding: 0;
        }

        nav {
            background-color: #ffffff;
            /* تغيير لون خلفية القائمة إلى الأبيض */
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 79px;
            padding: 0.5px;
            box-sizing: border-box;
            border-bottom: 1px solid #e0ede2;
            max-width: 12000px;
            display: flex;
            justify-content: flex-end;
            align-items: center;
            z-index: 1000;
        }

        nav ul {
            list-style: none;
            margin-right: 80px;
            position: relative;
            /* إضافة */
        }

        nav ul li a {
            line-height: 70px;
            color: #135313;
            text-decoration: none;
            font-size: 20px;
            font-weight: bold;
            text-transform: none;
        }

        .logo {
            position: absolute;
            top: -31px;
            left: 0;
        }

        .logo img {
            width: 65px;
            height: 65px;

        }

        custom-btn {
            background-color: #135313;
            /* لون أخضر داكن */
            color: #ffffff;
            /* لون أبيض للنص */
        }

        .custom-btn:active {
            background-color: #ffffff;
            /* لون الخلفية الأبيض */
            color: #135313;
            /* لون النص الأخضر */
        }

        body,
        html {
            height: 100vh;
            margin: 0;
            padding: 0;
        }

        .slick-custom {
            height: 100vh;
            /* أو أي ارتفاع ترغب فيه */
            margin: 0;
            /* إزالة الهوامش */
            padding: 0;
            /* إزالة الحواف الداخلية */

        }

        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f9f9f9;
            min-width: 160px;
            padding: 2px;
            border-radius: 20px;
            /* زيادة قيمة border-radius لجعل الحواف أكثر دائرية */
            box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
            z-index: 1;
        }

        .dropdown-content a {
            font-size: 14px;
            /* حجم النص */
            color: black;
            padding: 6px 16px;
            /* تباعد داخلي - تقليل التباعد الأفقي بشكل إضافي */
            text-decoration: none;
            display: block;
        }

        .dropdown-content a:hover {
            background-color: #f1f1f1;
        }

        .circle-outline {
            font-size: 25px;
        }

        .person-circle-outline {
            font-size: 25px;
        }

        .dropdown:hover .dropdown-content {
            display: block;
        }
    </style>
        <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">

</head>

<body>
    <nav>

        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{ route('transportation') }}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{ route('ser_mdn') }}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{ route('contact') }}">Contact</a></li>
                <li><a href="{{ route('aboutus') }}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{ route('new_notifications') }}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
            </div>
        </ul>

    </nav>
    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>

    <div class="main"> </div>

    <div class="header"> </div>

    <div class="message">
        Thank you, your request has been sent
    </div>


</body>
<footer>
    <div class="footer-icons">
        <a href="#"><i class="fa-brands fa-twitter"></i></a>
        <a href="#"><i class="fa-brands fa-tiktok"></i></a>
        <a href="#"><i class="fa-brands fa-instagram"></i></a>
    </div>
</footer>

</html>
