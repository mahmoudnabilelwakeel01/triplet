<!DOCTYPE html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title> Volunteering </title>
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('public/css/your.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/Volunteering.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset('public/css/head.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('public/css/head - Copy.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/foot.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.0/css/all.min.css"
        integrity="sha512-Avb2QiuDEEvB4bZJYdft2mNjVShBftLdPG8FJ0V7irTLQ8Uo0qcPxh4Plq7G5tGm0rU+1SPhVotteLpBERwTkw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@7.1.0/dist/ionicons/ionicons.esm.js"></script>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-6sq72aLhI3KqDlYoK3QCKTlzo+8kLlFfv1YH8LsLd00P1bt6pdrz/Zq8Fjz4gpi9" crossorigin="anonymous">

</head>

<body>
    <nav>
        <div class="logo">
            <a href="#"><img class="logo-image" src="{{ asset('public/img/logot.jpg') }}" alt="TripleT"></a>
        </div>
        <ul>

            <div class="menu">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li><a href="{{ route('bisc') }}">Your destination</a></li>
                <li><a href="{{ route('volunteering') }}">Volunteering</a></li>
                @isset(auth()->guard('web')->user()->type)
                    @if (auth()->guard('web')->user()->type == 3)
                        <li><a href="{{ route('companyoffer') }}">Offers</a></li>
                    @elseif(auth()->guard('web')->user()->type == 4)
                        <li><a href="{{ route('residentoffer') }}">Offers</a></li>
                    @endif
                @endisset

                <li><a href="{{ route('liveex') }}">Live Experience</a></li>
                <li>
                    <div class="nav-item dropdown" style="margin-top: 3%;">
                        <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown"
                            style="color: #135313; margin-top: -8px;">Service</a>
                        <div class="dropdown-menu m-0 bg-success rounded-0" style="height: 100px;">
                            <a href="{{route('transportation')}}" class="dropdown-item " style="height: 50%;">
                                <p style="font-size: 80%; margin-top: -15px;">Transportation</p>
                            </a>
                            <a href="{{route('ser_mdn')}}" class="dropdown-item" style="height: 50%">
                                <p style="font-size: 80%; margin-top: -15px;">Reservation</p>
                            </a>

                        </div>
                    </div>
                </li>
                <li><a href="{{route('contact')}}">Contact</a></li>
                <li><a href="{{route('aboutus')}}">About Us</a></li>
                <li style="line-height: 4;">
                    <a href="{{route('new_notifications')}}">
                        <i class="fa-solid fa-bell fa-lg" style="color: #38571a;"></i>
                    </a>
                </li>
                <li>
                    <div class="dropdown" style="top: 0;">
                        <a href="#"><ion-icon name="person-circle-outline"></ion-icon></a>
                        <div class="dropdown-content" style="width: max-content;">
                            <a href="{{route('profile')}}">My Account</a>
                            @isset(auth()->guard('web')->user()->type)
                                @if (auth()->guard('web')->user()->type == 3)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">


                                        <a href="{{ route('touristreqcompany') }}">Tourist request </a>
                                        <a href="{{ route('previoustouristreqcompany') }}">Previous Tourist request </a>

                                    </div>
                                @elseif(auth()->guard('web')->user()->type == 4)
                                    <div class="dropdown" onmouseover="showSubmenu()" onmouseout="hideSubmenu()">
                                        <a href="{{ route('bisresident') }}">Volunteer request </a>
                                        <a href="{{ route('previousbisresident') }}">Previous Volunteer request </a>

                                    </div>
                                @endif
                            @endisset
                            <a href="{{route('admin.logout')}}">Log out</a>
                        </div>
                    </div>
                </li>
                <script>
                    function showSubmenu() {
                        document.getElementById("submenu").style.display = "block";
                    }

                    function hideSubmenu() {
                        document.getElementById("submenu").style.display = "none";
                    }
                </script>
        </ul>

    </nav>

    <style>
        .dropdown ion-icon {
            font-size: 30px;
            margin-top: 15px;
        }
    </style>

    <br> <br> <br>
    <div class="container">
        <div class="section-title">
            <h1>Volunteering </h1>
        </div>
        <br><br>
        <div class="row">
            <div class="column">
                <div class="vol-9">
                    <div class="vol-img">
                        <img src="{{ asset('public/img/taa.jpg') }}" alt="vol Image">
                    </div>
                    <br>
                    <div class="vol-content">
                        <h2> Presentation </h2>
                        <h3> of volunteering</h3>
                    </div>
                    <div class="vol-overlay">
                        <p>Volunteering is the act of freely offering to do something without expecting payment or
                            compensation, usually for the benefit of others or a cause.</p>

                    </div>
                </div>
            </div>
            <div class="column">
                <div class="vol-9">
                    <div class="vol-img">
                        <img src="{{ asset('public/img/other.jpg') }}" alt="vol Image">
                    </div>
                    <br>
                    <div class="vol-content">
                        <h2>Volunteer work</h2>
                        <h3>in Saudi Arabia</h3>
                    </div>
                    <div class="vol-overlay">
                        <p>volunteer work in Saudi Arabia has grown significantly in recent years and has become an
                            important part of social life and sustainable development, as the government and society
                            have greatly supported it.</p>

                    </div>
                </div>
            </div>
            <div class="column">
                <div class="vol-9">
                    <div class="vol-img">
                        <img src="{{ asset('public/img/taa4.jpg') }}" alt="vol Image">
                    </div>
                    <br>
                    <div class="vol-content">
                        <h2>The importance</h2>

                        <h3>of volunteering</h3>
                    </div>
                    <div class="vol-overlay">
                        <p>"Volunteering is important because it contributes to improving society, developing
                            individuals' skills, enhancing a sense of belonging, and enhancing social values."</p>

                    </div>
                </div>
            </div>
            <div class="column">
                <div class="vol-9">
                    <div class="vol-img">
                        <img src="{{ asset('public/img/taa3.png') }}" alt="vol Image">
                    </div>
                    <br>
                    <div class="vol-content">
                        <h2>Live Experience</h2>

                        <h3>with TripleT</h3>
                    </div>
                    <div class="vol-overlay">
                        <p>Live the volunteer experience through the volunteer trips page and get several benefits.</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <br> <br>
</body>
<footer>
    <div class="footer-icons">
        <a href="#"><i class="fa-brands fa-twitter"></i></a>
        <a href="#"><i class="fa-brands fa-tiktok"></i></a>
        <a href="#"><i class="fa-brands fa-instagram"></i></a>
    </div>
</footer>

</html>
