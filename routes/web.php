<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdminLoginController;
use App\Http\Controllers\WebController;
use Illuminate\Support\Facades\Route;

// Route::get('/', function () {
//     return view('welcome');
// });


// Route::get('/', [WebController::class, 'home'])->name('home');
// Route::get('/your-destination', [WebController::class, 'bisc'])->name('bisc');
// Route::get('/volunteering', [WebController::class, 'volunteering'])->name('volunteering');
// Route::get('/live-experience', [WebController::class, 'liveex'])->name('liveex');
// Route::get('/live-experience/tourist-trip', [WebController::class, 'touristtrip'])->name('touristtrip');
// Route::get('/live-experience/tourist-trip/entertainment', [WebController::class, 'trip1'])->name('trip1');
// Route::get('/live-experience/tourist-trip/cultural', [WebController::class, 'trip2'])->name('trip2');
// Route::get('/live-experience/tourist-trip/religious', [WebController::class, 'trip3'])->name('trip3');
// Route::get('/live-experience/tourist-trip/sea', [WebController::class, 'trip4'])->name('trip4');
// Route::get('/live-experience/tourist-trip/environmental', [WebController::class, 'trip5'])->name('trip5');
// Route::get('/live-experience/tourist-trip/mountainous', [WebController::class, 'trip6'])->name('trip6');
// Route::get('/live-experience/tourist-trip/request/{id}', [WebController::class, 'triprequest'])->name('triprequest');
// Route::post('/live-experience/tourist-trip/request/sent/{id}', [WebController::class, 'triprequestsent'])->name('triprequestsent');
// Route::get('/live-experience/volunteer-trip', [WebController::class, 'volunteertrip'])->name('volunteertrip');
// Route::get('/live-experience/volunteer-trip/send/{id}', [WebController::class, 'voltrip_send'])->name('voltrip_send');
// Route::post('/live-experience/volunteer-trip/sent/{id}', [WebController::class, 'voltrip_sent'])->name('voltrip_sent');
// Route::get('/live-experience/volunteer-trip/education', [WebController::class, 'volunteertripeducation'])->name('volunteertripeducation');
// Route::get('/live-experience/volunteer-trip/education/english', [WebController::class, 'voltrip_edu_eng'])->name('voltrip_edu_eng');
// Route::get('/live-experience/volunteer-trip/education/chinese', [WebController::class, 'voltrip_edu_chin'])->name('voltrip_edu_chin');
// Route::get('/live-experience/volunteer-trip/education/french', [WebController::class, 'voltrip_edu_fren'])->name('voltrip_edu_fren');
// Route::get('/live-experience/volunteer-trip/education/hindi', [WebController::class, 'voltrip_edu_india'])->name('voltrip_edu_india');
// Route::get('/live-experience/volunteer-trip/education/turkish', [WebController::class, 'voltrip_edu_turk'])->name('voltrip_edu_turk');
// Route::get('/live-experience/volunteer-trip/education/russian', [WebController::class, 'voltrip_edu_russ'])->name('voltrip_edu_russ');
// Route::get('/live-experience/volunteer-trip/health', [WebController::class, 'volunteertripheal'])->name('volunteertripheal');
// Route::get('/live-experience/volunteer-trip/other', [WebController::class, 'volunteertripother'])->name('volunteertripother');
// Route::get('/service/transportation', [WebController::class, 'transportation'])->name('transportation');
// Route::get('/service/reservation', [WebController::class, 'ser_mdn'])->name('ser_mdn');
// Route::get('/service/reservation/riyadh', [WebController::class, 'serv_reserve_riyadh'])->name('serv_reserve_riyadh');
// Route::get('/service/reservation/jeddah', [WebController::class, 'serv_reserve_jeddah'])->name('serv_reserve_jeddah');
// Route::get('/service/reservation/makkah', [WebController::class, 'serv_reserve_makkah'])->name('serv_reserve_makkah');
// Route::get('/service/reservation/madenah', [WebController::class, 'serv_reserve_madenah'])->name('serv_reserve_madenah');
// Route::get('/contact', [WebController::class, 'contact'])->name('contact');
// Route::post('/contact/send', [WebController::class, 'contactsend'])->name('contactsend');
// Route::get('/about-us', [WebController::class, 'aboutus'])->name('aboutus');
// Route::get('/new-notifications', [WebController::class, 'new_notifications'])->name('new_notifications');
// Route::get('/delete/{id}', [WebController::class, 'delete'])->name('delete');
// Route::get('/evaluationp', [WebController::class, 'evaluationp'])->name('evaluationp');
// Route::get('/evaluation', [WebController::class, 'evaluation'])->name('evaluation');
// Route::get('/your-destination/jeddah', [WebController::class, 'jedd'])->name('jedd');
// Route::get('/your-destination/jeddah/the-best-restaurants-and-cafes', [WebController::class, 'jeddf'])->name('jeddf');
// Route::get('/your-destination/jeddah/places-of-residence', [WebController::class, 'jeddb'])->name('jeddb');
// Route::get('/your-destination/jeddah/best-tourist-attraction', [WebController::class, 'jeddc'])->name('jeddc');
// Route::get('/your-destination/al-aula', [WebController::class, 'ula'])->name('ula');
// Route::get('/your-destination/al-aula/the-best-restaurants-and-cafes', [WebController::class, 'ulab'])->name('ulab');
// Route::get('/your-destination/al-aula/places-of-residence', [WebController::class, 'ulac'])->name('ulac');
// Route::get('/your-destination/al-aula/best-tourist-attraction', [WebController::class, 'ulaf'])->name('ulaf');
// Route::get('/your-destination/mecca', [WebController::class, 'mecca'])->name('mecca');
// Route::get('/your-destination/mecca/the-best-restaurants-and-cafes', [WebController::class, 'meccab'])->name('meccab');
// Route::get('/your-destination/mecca/places-of-residence', [WebController::class, 'meccac'])->name('meccac');
// Route::get('/your-destination/mecca/best-tourist-attraction', [WebController::class, 'meccaf'])->name('meccaf');
// Route::get('/your-destination/riyadh', [WebController::class, 'riyadh'])->name('riyadh');
// Route::get('/your-destination/riyadh/the-best-restaurants-and-cafes', [WebController::class, 'riyadhb'])->name('riyadhb');
// Route::get('/your-destination/riyadh/places-of-residence', [WebController::class, 'riyadhc'])->name('riyadhc');
// Route::get('/your-destination/riyadh/best-tourist-attraction', [WebController::class, 'riyadhf'])->name('riyadhf');
// Route::get('/your-destination/al-madinah-aL-munawwarah', [WebController::class, 'madinah'])->name('madinah');
// Route::get('/your-destination/al-madinah-aL-munawwarah/the-best-restaurants-and-cafes', [WebController::class, 'madinahb'])->name('madinahb');
// Route::get('/your-destination/al-madinah-aL-munawwarah/places-of-residence', [WebController::class, 'madinahc'])->name('madinahc');
// Route::get('/your-destination/al-madinah-aL-munawwarah/best-tourist-attraction', [WebController::class, 'madinahf'])->name('madinahf');
// Route::get('/nature-and-adventure', [WebController::class, 'nature'])->name('nature');
// Route::get('/nature-and-adventure/museums/', [WebController::class, 'museums'])->name('museums');
// Route::get('/sea-and-beach', [WebController::class, 'sea'])->name('sea');
// Route::get('/sea-and-beach/beach', [WebController::class, 'beach'])->name('beach');
// Route::get('/culture-and-heritage', [WebController::class, 'culture'])->name('culture');
// Route::get('/culture-and-heritage/nature', [WebController::class, 'cultureNature'])->name('cultureNature');
// Route::get('/card', [WebController::class, 'card'])->name('card');
// Route::get('/card/sent', [WebController::class, 'cardsent'])->name('cardsent');
// Route::get('/request-history', [WebController::class, 'bis'])->name('bis');
// Route::get('/previous-request', [WebController::class, 'req'])->name('req');
// Route::get('/recent-request', [WebController::class, 'recent_req'])->name('recent_req');
// Route::get('/recent-request/accepted', [WebController::class, 'recent_req_accepted'])->name('recent_req_accepted');
// Route::get('/recent-request/rejected', [WebController::class, 'recent_req_rejected'])->name('recent_req_rejected');



// // admin routes

// Route::get('/login', [AdminLoginController::class, 'loginPage'])->name('admin.loginPage');
// Route::post('/login/attemp', [AdminLoginController::class, 'login'])->name('admin.loginattemp');
// Route::get('/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
// Route::get('/admin/register', [AdminLoginController::class, 'register'])->name('admin.register');
// Route::post('/admin/register/send', [AdminLoginController::class, 'registersend'])->name('admin.registersend');


// Route::prefix('company')->middleware(['auth', 'auth.session'])->group(function () {
//     // company
//     Route::get('/offer', [WebController::class, 'offer'])->name('offer');
//     Route::get('/offer/tourist', [WebController::class, 'offer_tourist'])->name('offer_tourist');
//     Route::get('/offer/tourist/add', [WebController::class, 'offer_tourist_add'])->name('offer_tourist_add');
//     Route::post('/offer/tourist/store', [WebController::class, 'offer_tourist_store'])->name('offer_tourist_store');
//     Route::get('/offer/tourist/edit/{id}', [WebController::class, 'offer_tourist_edit'])->name('offer_tourist_edit');
//     Route::post('/offer/tourist/update/{id}', [WebController::class, 'offer_tourist_update'])->name('offer_tourist_update');
//     Route::get('/offer/tourist/delete/{id}', [WebController::class, 'offer_tourist_delete'])->name('offer_tourist_delete');
//     Route::get('/offer/volunteer', [WebController::class, 'offer_volunteer'])->name('offer_volunteer');
//     Route::get('/offer/volunteer/add', [WebController::class, 'offer_volunteer_add'])->name('offer_volunteer_add');
//     Route::post('/offer/volunteer/store', [WebController::class, 'offer_volunteer_store'])->name('offer_volunteer_store');
//     Route::get('/offer/volunteer/edit/{id}', [WebController::class, 'offer_volunteer_edit'])->name('offer_volunteer_edit');
//     Route::post('/offer/volunteer/update/{id}', [WebController::class, 'offer_volunteer_update'])->name('offer_volunteer_update');
//     Route::get('/offer/volunteer/delete/{id}', [WebController::class, 'offer_volunteer_delete'])->name('offer_volunteer_delete');

//     Route::get('/request-history', [WebController::class, 'biscompany'])->name('biscompany');
//     Route::get('/request-history/detail/{id}', [WebController::class, 'biscompanydetail'])->name('biscompanydetail');
//     Route::get('/request-history/accept/{id}', [WebController::class, 'biscompanyaccept'])->name('biscompanyaccept');
//     Route::get('/request-history/reject/{id}', [WebController::class, 'biscompanyreject'])->name('biscompanyreject');
//     Route::get('/previous-request', [WebController::class, 'reqcompany'])->name('reqcompany');
//     Route::get('/previous-request/delete/{id}', [WebController::class, 'reqcompanydelete'])->name('reqcompanydelete');
// });


//admin
// Route::prefix('admin')->name('admin.')->middleware(['auth', 'auth.session'])->group(function () {
//     Route::get('/service', [AdminController::class, 'service'])->name('service');
//     Route::get('/service/transportation', [AdminController::class, 'serv_transportation'])->name('serv_transportation');
//     Route::get('/service/transportation/add', [AdminController::class, 'serv_transportation_add'])->name('serv_transportation_add');
//     Route::post('/service/transportation/store', [AdminController::class, 'serv_transportation_store'])->name('serv_transportation_store');
//     Route::get('/service/transportation/edit/{id}', [AdminController::class, 'serv_transportation_edit'])->name('serv_transportation_edit');
//     Route::post('/service/transportation/update/{id}', [AdminController::class, 'serv_transportation_update'])->name('serv_transportation_update');
//     Route::get('/service/transportation/delete/{id}', [AdminController::class, 'serv_transportation_delete'])->name('serv_transportation_delete');
//     Route::get('/service/reservation', [AdminController::class, 'serv_reservation'])->name('serv_reservation');
//     Route::get('/service/reservation/add', [AdminController::class, 'serv_reservation_add'])->name('serv_reservation_add');
//     Route::post('/service/reservation/store', [AdminController::class, 'serv_reservation_store'])->name('serv_reservation_store');
//     Route::get('/service/reservation/edit/{id}', [AdminController::class, 'serv_reservation_edit'])->name('serv_reservation_edit');
//     Route::post('/service/reservation/update/{id}', [AdminController::class, 'serv_reservation_update'])->name('serv_reservation_update');
//     Route::get('/service/reservation/delete/{id}', [AdminController::class, 'serv_reservation_delete'])->name('serv_reservation_delete');
//     Route::get('/contact', [AdminController::class, 'contact'])->name('contact');
//     Route::get('/contact/delete/{id}', [AdminController::class, 'contactdelete'])->name('contactdelete');
// });




// admin routes
Route::get('/login', [AdminLoginController::class, 'loginPage'])->name('admin.loginPage');
Route::post('/login/attemp', [AdminLoginController::class, 'login'])->name('admin.loginattemp');
Route::get('/logout', [AdminLoginController::class, 'logout'])->name('admin.logout');
Route::get('/admin/register', [AdminLoginController::class, 'register'])->name('admin.register');
Route::post('/admin/register/send', [AdminLoginController::class, 'registersend'])->name('admin.registersend');

Route::middleware(['auth', 'auth.session'])->group(function () {
    Route::get('/new-notifications', [WebController::class, 'new_notifications'])->name('new_notifications');
    // admin
    Route::prefix('admin')->name('admin.')->middleware(['adminCheck'])->group(function () {
        Route::get('/service', [AdminController::class, 'service'])->name('service');
        Route::get('/service/transportation', [AdminController::class, 'serv_transportation'])->name('serv_transportation');
        Route::get('/service/transportation/add', [AdminController::class, 'serv_transportation_add'])->name('serv_transportation_add');
        Route::post('/service/transportation/store', [AdminController::class, 'serv_transportation_store'])->name('serv_transportation_store');
        Route::get('/service/transportation/edit/{id}', [AdminController::class, 'serv_transportation_edit'])->name('serv_transportation_edit');
        Route::post('/service/transportation/update/{id}', [AdminController::class, 'serv_transportation_update'])->name('serv_transportation_update');
        Route::get('/service/transportation/delete/{id}', [AdminController::class, 'serv_transportation_delete'])->name('serv_transportation_delete');
        Route::get('/service/reservation', [AdminController::class, 'serv_reservation'])->name('serv_reservation');
        Route::get('/service/reservation/add', [AdminController::class, 'serv_reservation_add'])->name('serv_reservation_add');
        Route::post('/service/reservation/store', [AdminController::class, 'serv_reservation_store'])->name('serv_reservation_store');
        Route::get('/service/reservation/edit/{id}', [AdminController::class, 'serv_reservation_edit'])->name('serv_reservation_edit');
        Route::post('/service/reservation/update/{id}', [AdminController::class, 'serv_reservation_update'])->name('serv_reservation_update');
        Route::get('/service/reservation/delete/{id}', [AdminController::class, 'serv_reservation_delete'])->name('serv_reservation_delete');
        Route::get('/contact', [AdminController::class, 'contact'])->name('contact');
        Route::get('/contact/response/{id}', [AdminController::class, 'contactresponse'])->name('contactresponse');
        Route::post('/contact/response/save/{id}', [AdminController::class, 'contactresponsesave'])->name('contactresponsesave');
        Route::get('/contact/delete/{id}', [AdminController::class, 'contactdelete'])->name('contactdelete');

    });
    Route::get('/profile', [WebController::class, 'profile'])->name('profile');
    Route::post('/profile/save', [WebController::class, 'profileSave'])->name('profileSave');

    // user
    Route::middleware(['userCheck'])->group(function () {
        Route::get('/live-experience/tourist-trip/request/{id}', [WebController::class, 'triprequest'])->name('triprequest');
        Route::post('/live-experience/tourist-trip/request/sent/{id}', [WebController::class, 'triprequestsent'])->name('triprequestsent');
        Route::get('/live-experience/volunteer-trip/send/{type}/{id}', [WebController::class, 'voltrip_send'])->name('voltrip_send');
        Route::post('/live-experience/volunteer-trip/sent/{type}/{id}', [WebController::class, 'voltrip_sent'])->name('voltrip_sent');
    });

    // company
    Route::prefix('company')->middleware(['companyCheck'])->group(function () {
        // company
        Route::get('/offer', [WebController::class, 'offer'])->name('companyoffer');
        Route::get('/offer/tourist', [WebController::class, 'offer_tourist'])->name('offer_tourist');
        Route::get('/offer/tourist/add', [WebController::class, 'offer_tourist_add'])->name('offer_tourist_add');
        Route::post('/offer/tourist/store', [WebController::class, 'offer_tourist_store'])->name('offer_tourist_store');
        Route::get('/offer/tourist/edit/{id}', [WebController::class, 'offer_tourist_edit'])->name('offer_tourist_edit');
        Route::post('/offer/tourist/update/{id}', [WebController::class, 'offer_tourist_update'])->name('offer_tourist_update');
        Route::get('/offer/tourist/delete/{id}', [WebController::class, 'offer_tourist_delete'])->name('offer_tourist_delete');
        Route::get('/tourist-request', [WebController::class, 'touristreqcompany'])->name('touristreqcompany');
        Route::get('/previous-tourist-request', [WebController::class, 'previoustouristreqcompany'])->name('previoustouristreqcompany');
        Route::get('/previous-request', [WebController::class, 'reqcompany'])->name('reqcompany');
        Route::get('/previous-request/delete/{id}', [WebController::class, 'reqcompanydelete'])->name('reqcompanydelete');

        Route::get('/request-history/company/accept/{id}', [WebController::class, 'biscompanyaccept'])->name('biscompanyaccept');
        Route::get('/request-history/company/reject/{id}', [WebController::class, 'biscompanyreject'])->name('biscompanyreject');

    });


    // resident
    Route::prefix('resident')->middleware(['residentCheck'])->group(function () {
        Route::get('/offer', [WebController::class, 'offer'])->name('residentoffer');
        Route::get('/offer/volunteer', [WebController::class, 'offer_volunteer'])->name('offer_volunteer');
        Route::get('/offer/volunteer/add', [WebController::class, 'offer_volunteer_add'])->name('offer_volunteer_add');
        Route::post('/offer/volunteer/store', [WebController::class, 'offer_volunteer_store'])->name('offer_volunteer_store');
        Route::get('/offer/volunteer/edit/{type}/{id}', [WebController::class, 'offer_volunteer_edit'])->name('offer_volunteer_edit');
        Route::post('/offer/volunteer/update/{type}/{id}', [WebController::class, 'offer_volunteer_update'])->name('offer_volunteer_update');
        Route::get('/offer/volunteer/delete/{type}/{id}', [WebController::class, 'offer_volunteer_delete'])->name('offer_volunteer_delete');


        Route::get('/request-history', [WebController::class, 'bisresident'])->name('bisresident');
        Route::get('/request-history/detail/{id}', [WebController::class, 'bisresidentdetail'])->name('bisresidentdetail');
        Route::get('/request-history/resident/accept/{id}', [WebController::class, 'bisresidentaccept'])->name('bisresidentaccept');
        Route::get('/request-history/resident/reject/{id}', [WebController::class, 'bisresidentreject'])->name('bisresidentreject');
        Route::get('/previous-request-history', [WebController::class, 'previousbisresident'])->name('previousbisresident');

    });
    // Route::get('/request-history', [WebController::class, 'bis'])->name('bis');
    // Route::get('/previous-request', [WebController::class, 'req'])->name('req');
    // Route::get('/recent-request', [WebController::class, 'recent_req'])->name('recent_req');
});

Route::get('/', [WebController::class, 'home'])->name('home');
Route::get('/your-destination', [WebController::class, 'bisc'])->name('bisc');
Route::get('/volunteering', [WebController::class, 'volunteering'])->name('volunteering');
Route::get('/live-experience', [WebController::class, 'liveex'])->name('liveex');
Route::get('/live-experience/tourist-trip', [WebController::class, 'touristtrip'])->name('touristtrip');
Route::get('/live-experience/tourist-trip/entertainment', [WebController::class, 'trip1'])->name('trip1');
Route::get('/live-experience/tourist-trip/cultural', [WebController::class, 'trip2'])->name('trip2');
Route::get('/live-experience/tourist-trip/religious', [WebController::class, 'trip3'])->name('trip3');
Route::get('/live-experience/tourist-trip/sea', [WebController::class, 'trip4'])->name('trip4');
Route::get('/live-experience/tourist-trip/environmental', [WebController::class, 'trip5'])->name('trip5');
Route::get('/live-experience/tourist-trip/mountainous', [WebController::class, 'trip6'])->name('trip6');
Route::get('/live-experience/volunteer-trip', [WebController::class, 'volunteertrip'])->name('volunteertrip');
Route::get('/live-experience/volunteer-trip/education', [WebController::class, 'volunteertripeducation'])->name('volunteertripeducation');
Route::get('/live-experience/volunteer-trip/education/english', [WebController::class, 'voltrip_edu_eng'])->name('voltrip_edu_eng');
Route::get('/live-experience/volunteer-trip/education/chinese', [WebController::class, 'voltrip_edu_chin'])->name('voltrip_edu_chin');
Route::get('/live-experience/volunteer-trip/education/french', [WebController::class, 'voltrip_edu_fren'])->name('voltrip_edu_fren');
Route::get('/live-experience/volunteer-trip/education/hindi', [WebController::class, 'voltrip_edu_india'])->name('voltrip_edu_india');
Route::get('/live-experience/volunteer-trip/education/turkish', [WebController::class, 'voltrip_edu_turk'])->name('voltrip_edu_turk');
Route::get('/live-experience/volunteer-trip/education/russian', [WebController::class, 'voltrip_edu_russ'])->name('voltrip_edu_russ');
Route::get('/live-experience/volunteer-trip/health', [WebController::class, 'volunteertripheal'])->name('volunteertripheal');
Route::get('/live-experience/volunteer-trip/other', [WebController::class, 'volunteertripother'])->name('volunteertripother');
Route::get('/service/transportation', [WebController::class, 'transportation'])->name('transportation');
Route::get('/service/reservation', [WebController::class, 'ser_mdn'])->name('ser_mdn');
Route::get('/service/reservation/riyadh', [WebController::class, 'serv_reserve_riyadh'])->name('serv_reserve_riyadh');
Route::get('/service/reservation/jeddah', [WebController::class, 'serv_reserve_jeddah'])->name('serv_reserve_jeddah');
Route::get('/service/reservation/makkah', [WebController::class, 'serv_reserve_makkah'])->name('serv_reserve_makkah');
Route::get('/service/reservation/madenah', [WebController::class, 'serv_reserve_madenah'])->name('serv_reserve_madenah');
Route::get('/contact', [WebController::class, 'contact'])->name('contact');
Route::post('/contact/send', [WebController::class, 'contactsend'])->name('contactsend');
Route::get('/about-us', [WebController::class, 'aboutus'])->name('aboutus');
Route::get('/your-destination/jeddah', [WebController::class, 'jedd'])->name('jedd');
Route::get('/your-destination/jeddah/the-best-restaurants-and-cafes', [WebController::class, 'jeddf'])->name('jeddf');
Route::get('/your-destination/jeddah/places-of-residence', [WebController::class, 'jeddb'])->name('jeddb');
Route::get('/your-destination/jeddah/best-tourist-attraction', [WebController::class, 'jeddc'])->name('jeddc');
Route::get('/your-destination/al-aula', [WebController::class, 'ula'])->name('ula');
Route::get('/your-destination/al-aula/the-best-restaurants-and-cafes', [WebController::class, 'ulab'])->name('ulab');
Route::get('/your-destination/al-aula/places-of-residence', [WebController::class, 'ulac'])->name('ulac');
Route::get('/your-destination/al-aula/best-tourist-attraction', [WebController::class, 'ulaf'])->name('ulaf');
Route::get('/your-destination/mecca', [WebController::class, 'mecca'])->name('mecca');
Route::get('/your-destination/mecca/the-best-restaurants-and-cafes', [WebController::class, 'meccab'])->name('meccab');
Route::get('/your-destination/mecca/places-of-residence', [WebController::class, 'meccac'])->name('meccac');
Route::get('/your-destination/mecca/best-tourist-attraction', [WebController::class, 'meccaf'])->name('meccaf');
Route::get('/your-destination/riyadh', [WebController::class, 'riyadh'])->name('riyadh');
Route::get('/your-destination/riyadh/the-best-restaurants-and-cafes', [WebController::class, 'riyadhb'])->name('riyadhb');
Route::get('/your-destination/riyadh/places-of-residence', [WebController::class, 'riyadhc'])->name('riyadhc');
Route::get('/your-destination/riyadh/best-tourist-attraction', [WebController::class, 'riyadhf'])->name('riyadhf');
Route::get('/your-destination/al-madinah-aL-munawwarah', [WebController::class, 'madinah'])->name('madinah');
Route::get('/your-destination/al-madinah-aL-munawwarah/the-best-restaurants-and-cafes', [WebController::class, 'madinahb'])->name('madinahb');
Route::get('/your-destination/al-madinah-aL-munawwarah/places-of-residence', [WebController::class, 'madinahc'])->name('madinahc');
Route::get('/your-destination/al-madinah-aL-munawwarah/best-tourist-attraction', [WebController::class, 'madinahf'])->name('madinahf');
Route::get('/nature-and-adventure', [WebController::class, 'nature'])->name('nature');
Route::get('/nature-and-adventure/museums/', [WebController::class, 'museums'])->name('museums');
Route::get('/sea-and-beach', [WebController::class, 'sea'])->name('sea');
Route::get('/sea-and-beach/beach', [WebController::class, 'beach'])->name('beach');
Route::get('/culture-and-heritage', [WebController::class, 'culture'])->name('culture');
Route::get('/culture-and-heritage/nature', [WebController::class, 'cultureNature'])->name('cultureNature');
Route::get('/card', [WebController::class, 'card'])->name('card');
Route::get('/card/sent', [WebController::class, 'cardsent'])->name('cardsent');
Route::get('/recent-request/accepted', [WebController::class, 'recent_req_accepted'])->name('recent_req_accepted');
Route::get('/recent-request/rejected', [WebController::class, 'recent_req_rejected'])->name('recent_req_rejected');
Route::get('/delete/{id}', [WebController::class, 'delete'])->name('delete');
Route::get('/evaluationp', [WebController::class, 'evaluationp'])->name('evaluationp');
Route::get('/evaluation', [WebController::class, 'evaluation'])->name('evaluation');
